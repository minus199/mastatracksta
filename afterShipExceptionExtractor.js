/* 
 * Copyright 2015 Your Organisation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function extractData() {
    var output = [];
    $(".table-condensed tbody tr").each(function (trIndex, tr) {
        var $trChildren = $(tr).children("td");
        
        var $httpStatusCode = $trChildren.eq(0);
        var $metaCode = $trChildren.eq(1);
        var $metaType = $trChildren.eq(2);
        var $errorMessage = $trChildren.eq(3);

        if ($httpStatusCode.children("code").length > 1) {
            $httpStatusCode.children("code").each(function (codeIndex, code) {
                output.push(extractDetails($(code), $(code), $metaType, $errorMessage));
            });
        } else {
            output.push(extractDetails($httpStatusCode, $metaCode, $metaType, $errorMessage));
        }
    });
    
    return output;
}

str.replace(/\"/g, "\\\"");


function extractDetails($httpStatusCode , $metaCode , $metaType , $errorMessage ){
    return {http_status_code: $httpStatusCode.text(),  meta_code: $metaCode.text(),  meta_type: $metaType.text(), message: $errorMessage.text()};
}