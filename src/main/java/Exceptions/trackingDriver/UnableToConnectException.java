package Exceptions.trackingDriver;

/**
 * @author minus
 */
public class UnableToConnectException extends Exception{
    final private Exception causedBy;

    public UnableToConnectException(Exception previousException) {
        this.causedBy = previousException;
    }

    public Exception getCausedBy() {
        return causedBy;
    }
}
