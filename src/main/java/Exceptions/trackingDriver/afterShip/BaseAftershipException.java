/*
 * Copyright 2015 Your Organisation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package Exceptions.trackingDriver.afterShip;

import Classes.AftershipAPIException;
import com.google.gson.Gson;
import java.util.HashMap;

/**
 *
 * @author asafb
 */
public class BaseAftershipException extends Exception {

    final private AftershipAPIException aftershipAPIException;
    private AftershipServerException aftershipServerException;

    public BaseAftershipException(AftershipAPIException aftershipAPIException) {
        this.aftershipAPIException = aftershipAPIException;
        parse();
    }

    public BaseAftershipException(String message) {
        super(message);
        this.aftershipAPIException = null;
    }

    private void parse() {
        Gson gson = new Gson();
        aftershipServerException = gson.fromJson(aftershipAPIException.getMessage(), AftershipServerException.class);
        setStackTrace(aftershipAPIException.getStackTrace());
    }

    private static class AftershipServerException {
        private HashMap meta;

        public AftershipServerException() {

        }

        public Double getErrorCode() {
            return (Double) meta.get("code");
        }

        public String getMessage() {
            return (String) meta.get("message");
        }

        public String getType() {
            return (String) meta.get("type");
        }
    }

    public Double getErrorCode() {
        return aftershipServerException.getErrorCode();
    }

    @Override
    public String getMessage() {
        return aftershipServerException.getMessage();
    }

    public String getType() {
        return aftershipServerException.getType();
    }

    public AftershipAPIException getPreviousException() {
        return aftershipAPIException;
    }
}
