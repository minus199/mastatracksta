package Exceptions.trackingDriver.afterShip;

import Classes.AftershipAPIException;
import Classes.Tracking;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;

/**
 * @author minus
 */
public class MoreThanOneCourierDetectedException extends BaseAftershipException{
    public MoreThanOneCourierDetectedException() {
        super("More than one courier detected");
    }
}
