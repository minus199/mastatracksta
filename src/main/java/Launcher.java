
import Exceptions.trackingDriver.UnableToConnectException;
import Utils.MsgGen;
import com.minus.mastatracksta.DB.DBManager;
import com.minus.mastatracksta.MainApp;
import com.sun.javafx.application.LauncherImpl;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mastatrackstapreloader.MastaTrackstaPreloader;

/**
 * @author minus
 */
public class Launcher {

    public static void main(String[] args) throws UnableToConnectException {
        launchSequence(args);

        /*try {
            GmailQuickstart gmailQuickstart = new GmailQuickstart();
            gmailQuickstart.run();
        } catch (IOException ex) {
            Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
        }*/

        registerShutdown();
    }
    
    private static void launchSequence(String[] args){
        try {
            DBManager.getInstance();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
            MsgGen.alertException(ex);
            System.exit(1);
        }
        
        LauncherImpl.launchApplication(MainApp.class, MastaTrackstaPreloader.class, args);
    }

    private static void registerShutdown() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("ENTER SHUTDOWN");
                try {
                    DBManager.getInstance().closeConnection();
                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}
