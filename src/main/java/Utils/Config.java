package Utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

/**
 * @author minus
 */
public class Config {

    final public static String RSC_CONFIG_PATH = "config.properties";
    final private HashMap<String, Properties> cachedProperties = new HashMap();
    final private String activeFilePath;

    private Config() throws IOException, Exception {
        this.activeFilePath = RSC_CONFIG_PATH;
        load();
    }

    public static Config getInstance() throws IOException, Exception {
        return getInstance(RSC_CONFIG_PATH);
    }

    public static Config getInstance(String configFile) throws IOException, Exception {
        Config instance = new Config(configFile);
        return instance;
    }

    private Config(String configFile) throws IOException, Exception {
        this.activeFilePath = configFile;
        load(configFile);
    }

    public Config setConfigItem(String key, String value) {
        cachedProperties.get(activeFilePath).setProperty(key, value);

        return this;
    }

    public String getConfigValue(String key) {
        return cachedProperties.get(activeFilePath).getProperty(key);
    }

    private Config update() throws FileNotFoundException, IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(activeFilePath);
        cachedProperties.get(activeFilePath).store(fileOutputStream, "last update " + new Date().toString());

        return this;
    }

    private Config load() throws IOException, Exception {
        return load(RSC_CONFIG_PATH);
    }

    private Config load(String configFilePath) throws IOException, Exception {
        if (cachedProperties.get(configFilePath) != null) {
            return this;
        }

        InputStream input = Config.class.getClassLoader().getResourceAsStream(configFilePath);
        if (input == null) {
            throw new Exception("Unable to load config file " + configFilePath);
        }

        Properties properties = new Properties();
        properties.load(input);
        cachedProperties.put(configFilePath, properties);

        return this;
    }
}
