package com.minus.mastatracksta.DB;

import Utils.MsgGen;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author minus
 */
public enum DBManager {
    INSTANCE;
    private ConnectionSource connectionSource = null;
    

    private DBManager initConnection() {
        if (connectionSource == null) {
            try {
                Class.forName("org.sqlite.JDBC");
                connectionSource = new JdbcConnectionSource("jdbc:sqlite:MastaTracksta.packages");
                TableCreator.createAllIfNotExists(connectionSource);
                DtoFactory.getInstance().setConnectionSource(connectionSource);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
                MsgGen.alertException(ex);
            }
        }

        return this;
    }

    public <T> T getDao(Class<T> classModel) throws IllegalAccessException, InstantiationException, SQLException {
        return DaoManager.createDao(connectionSource, classModel);
    }

    public static DBManager getInstance() throws SQLException, ClassNotFoundException {
        return INSTANCE.initConnection();
    }
    
    public void closeConnection(){
        try {
            connectionSource.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
