/*
 * Copyright 2015 Your Organisation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.minus.mastatracksta.DB.DTO;

import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelAftershipException;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author asafb
 */
public class AftershipExceptionDTO extends BaseDTO{
     private static AftershipExceptionDTO instance;

    private AftershipExceptionDTO() throws SQLException {
        dao = DtoFactory.getInstance().getAftershipExceptionDao();
    }

    public static AftershipExceptionDTO getInstance() throws SQLException {
        if (instance == null) {
            instance = new AftershipExceptionDTO();
        }

        return instance;
    }
    
    public ModelAftershipException getByID(Integer id) throws SQLException{
        return (ModelAftershipException) dao.queryForId(id);
    }
    
    public List<ModelAftershipException> getByHttpCode(Double httpCode) throws SQLException{
        return dao.queryForEq("httpCode", httpCode);
    }
    
    public List<ModelAftershipException> getByCode(Double code) throws SQLException{
        return dao.queryForEq("code", code);
    }
    
    public List<ModelAftershipException> getByType(String type) throws SQLException{
        return dao.queryForEq("type", type);
    }
}
