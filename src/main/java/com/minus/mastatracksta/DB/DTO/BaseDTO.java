package com.minus.mastatracksta.DB.DTO;

import com.j256.ormlite.dao.Dao;
import com.minus.mastatracksta.DB.Models.BaseModel;
import java.sql.SQLException;
import java.util.List;

/**
 * @author minus
 */
abstract public class BaseDTO {

    protected Dao dao;

    protected BaseModel getFirstInList(List<BaseModel> models) {
        if (models == null || models.size() < 1) {
            return null;
        }

        return models.get(0);
    }

    public Boolean isEmpty() throws SQLException {
        return !(dao.countOf() > 0);
    }
    
    public Dao.CreateOrUpdateStatus save(BaseModel baseModel) throws SQLException{
        return dao.createOrUpdate(baseModel);
    }
}
