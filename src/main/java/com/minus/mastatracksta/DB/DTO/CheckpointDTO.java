package com.minus.mastatracksta.DB.DTO;

import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelCheckpoint;
import com.minus.mastatracksta.DB.Models.ModelTracking;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author minus
 */
public class CheckpointDTO extends BaseDTO {

    private static CheckpointDTO instance;

    private CheckpointDTO() throws SQLException {
        dao = DtoFactory.getInstance().getCheckpointDao();
    }

    public static CheckpointDTO getInstance() throws SQLException {
        if (instance == null) {
            instance = new CheckpointDTO();
        }

        return instance;
    }

    public Boolean isExists(ModelCheckpoint modelCheckpoint) throws SQLException {
        ArrayList<ModelCheckpoint> modelCheckpoints = (ArrayList<ModelCheckpoint>) DtoFactory.getInstance().getCheckpointDao().queryForMatchingArgs(modelCheckpoint);
        
        return modelCheckpoints.size() > 0;
    }
    
    public List<ModelCheckpoint> getByTracking(ModelTracking tracking) throws SQLException{
        return dao.queryForEq("tracking_id", tracking.getId());
    }
}
