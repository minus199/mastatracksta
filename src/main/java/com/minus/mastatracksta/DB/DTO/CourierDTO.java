package com.minus.mastatracksta.DB.DTO;

import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelCourier;
import java.sql.SQLException;
import java.util.List;

/**
 * @author minus
 */
public class CourierDTO extends BaseDTO {

    private static CourierDTO instance;

    private CourierDTO() throws Exception {
        dao = DtoFactory.getInstance().getCouriorDao();
    }

    public static CourierDTO getInstance() throws Exception {
        if (instance == null) {
            instance = new CourierDTO();
        }

        return instance;
    }

    public ModelCourier getByID(Integer courierID) throws SQLException {
        return (ModelCourier) dao.queryForId(courierID);
    }

    public ModelCourier getBySlug(String slug) throws SQLException {
        return (ModelCourier) dao.queryForEq("slug", slug).get(0);
    }

    public List<ModelCourier> getByName(String name) throws SQLException {
        return dao.queryForEq("name", name);
    }

    public List<ModelCourier> getByOtherName(String name) throws SQLException {
        return dao.queryForEq("other_name", name);
    }
    
    public List<ModelCourier> getAll() throws SQLException{
        return dao.queryForAll();
    }
}
