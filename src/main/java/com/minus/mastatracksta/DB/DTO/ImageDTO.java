package com.minus.mastatracksta.DB.DTO;

import com.j256.ormlite.dao.Dao;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.BaseModel;
import com.minus.mastatracksta.DB.Models.ModelImage;
import java.sql.SQLException;

/**
 * @author minus
 */
public class ImageDTO extends BaseDTO{
    private Dao dao;
    private static ImageDTO instance;

    private ImageDTO() throws SQLException {
        this.dao = DtoFactory.getInstance().getImageDao();
    }
    
    public static ImageDTO getInstance() throws SQLException{
        if (instance == null){
            instance = new ImageDTO();
        }
        
        return instance;
    }
    
    public ModelImage getByID(Integer id) throws SQLException{
        return (ModelImage) dao.queryForId(id);
    }
    
    public ModelImage getByHash(String hash) throws SQLException{
        BaseModel firstInList = getFirstInList(dao.queryForEq("imageName", hash));
        return (ModelImage) firstInList;
    }
}
