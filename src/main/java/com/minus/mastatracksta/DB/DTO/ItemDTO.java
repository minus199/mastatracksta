package com.minus.mastatracksta.DB.DTO;

import com.j256.ormlite.dao.Dao;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.DB.Models.ModelPackageItems;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.ShippingPackage.PackageFactory;
import java.sql.SQLException;

/**
 * @author minus
 */
public class ItemDTO extends BaseDTO {
    private static ItemDTO instance;

    private ItemDTO() throws SQLException {
        dao = DtoFactory.getInstance().getItemDao();
    }

    public static ItemDTO getInstance() throws SQLException {
        if (instance == null) {
            instance = new ItemDTO();
        }

        return instance;
    }

    public ModelItem getByID(Integer id) throws SQLException {
        return (ModelItem) dao.queryForId(id);
    }
    
    public ModelItem getByItemCode(String itemCode) throws SQLException {
        return (ModelItem) getFirstInList(dao.queryForEq("itemCode", itemCode));
    }
    
    public OriginEnum getOrigin(ModelItem modelItem) throws SQLException, IllegalAccessException, InstantiationException, Exception{
        ModelPackageItems modelPackageItem = PackageItemsDTO.getInstance().getByItemID(modelItem.getId());
        if (modelPackageItem == null)
            return null;

        com.minus.mastatracksta.ShippingPackage.Package pkg = PackageFactory.getBy().id(modelPackageItem.getPkg().getId());
        return pkg.getOrigin();
    }
    
    public OriginEnum getOrigin(Integer itemID) throws SQLException, IllegalAccessException, InstantiationException, Exception{
        return getOrigin(getByID(itemID));
    }
}
