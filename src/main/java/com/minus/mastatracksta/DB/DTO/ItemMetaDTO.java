package com.minus.mastatracksta.DB.DTO;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.BaseModel;
import com.minus.mastatracksta.DB.Models.ModelItemMeta;
import com.minus.mastatracksta.Modules.ENUMS.BaseMetaEnum;
import java.sql.SQLException;
import java.util.List;

/**
 * @author minus
 */
public class ItemMetaDTO extends BaseDTO {

    private static ItemMetaDTO instance;

    private ItemMetaDTO() throws SQLException {
        dao = DtoFactory.getInstance().getItemMetaDao();
    }
    
    public static ItemMetaDTO getInstance() throws SQLException {
        if (instance == null) {
            instance = new ItemMetaDTO();
        }

        return instance;
    }
    
    public ModelItemMeta getByItemIdAndMetaKey(Integer itemID, BaseMetaEnum metaKey) throws SQLException{
        QueryBuilder builder = dao.queryBuilder();
        Where where = builder.where();
        where.eq("itemID", itemID).and().eq("metaKey", metaKey);

        BaseModel firstInList = getFirstInList(dao.query(builder.prepare()));
        return (ModelItemMeta) firstInList;
    }
    
    public List<ModelItemMeta> getByItemId(Integer itemID) throws SQLException{
        return dao.queryForEq("itemID", itemID);
    }
}
