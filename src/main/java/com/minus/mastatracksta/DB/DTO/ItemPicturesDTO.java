package com.minus.mastatracksta.DB.DTO;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.BaseModel;
import com.minus.mastatracksta.DB.Models.ModelItemPicture;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author minus
 */
public class ItemPicturesDTO extends BaseDTO {

    private static ItemPicturesDTO instance;

    private ItemPicturesDTO() throws SQLException, ClassNotFoundException {
        dao = DtoFactory.getInstance().getItemPicturesDao();
    }

    public static ItemPicturesDTO getInstance() throws SQLException, ClassNotFoundException {
        if (instance == null) {
            instance = new ItemPicturesDTO();
        }

        return instance;
    }

    public ModelItemPicture getByID(Integer id) throws SQLException {
        return (ModelItemPicture) dao.queryForId(id);
    }

    public ModelItemPicture getByItemIdAndImgID(Integer itemID, Integer imageID) throws SQLException {
        QueryBuilder builder = dao.queryBuilder();
        Where where = builder.where();
        where.eq("item_id", itemID).and().eq("image_id", imageID);

        BaseModel firstInList = getFirstInList(dao.query(builder.prepare()));
        return (ModelItemPicture) firstInList;
    }

    public List<ModelItemPicture> getByItemID(Integer itemID) throws SQLException {
        List byItemID = dao.queryForEq("item_id", itemID);
        byItemID.stream().forEach(model -> {
            try {
                Integer id = ((ModelItemPicture)model).getModelImage().getId();
                ((ModelItemPicture)model).setModelImage(ImageDTO.getInstance().getByID(id));
            } catch (SQLException ex) {
                Logger.getLogger(ItemPicturesDTO.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        return byItemID;
    }
}
