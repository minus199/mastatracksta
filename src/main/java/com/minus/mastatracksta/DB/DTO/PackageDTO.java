package com.minus.mastatracksta.DB.DTO;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.DB.Models.ModelPackage;
import com.minus.mastatracksta.DB.Models.ModelPackageItems;
import com.minus.mastatracksta.ShippingPackage.Item.resolvers.ByOriginItemResolver;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author minus
 */
public class PackageDTO extends BaseDTO{
    private static PackageDTO instance;

    private PackageDTO() throws SQLException {
        dao = DtoFactory.getInstance().getPackagesDao();
    }

    public static PackageDTO getInstance() throws SQLException {
        if (instance == null)
            instance = new PackageDTO();
        
        return instance;
    }
    
    public ModelPackage getByID(Integer itemID) throws SQLException{
        return (ModelPackage) dao.queryForId(itemID);
    }
    
    public List<ModelPackageItems> getModelPackageItems(Integer packageID) throws SQLException{
        return PackageItemsDTO.getInstance().getByPackageID(packageID);
    }
    
    public ModelPackage getByTrackingNumber(String trackingNumber) throws SQLException{
        return (ModelPackage) dao.queryForEq("trackingNumber", trackingNumber).get(0);
    }
}
