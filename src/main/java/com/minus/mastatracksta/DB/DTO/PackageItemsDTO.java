package com.minus.mastatracksta.DB.DTO;

import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelPackageItems;
import java.sql.SQLException;
import java.util.List;

/**
 * @author minus
 */
public class PackageItemsDTO extends BaseDTO {

    private static PackageItemsDTO instance;

    private PackageItemsDTO() throws SQLException {
        dao = DtoFactory.getInstance().getPackageItemsDao();
    }

    public static PackageItemsDTO getInstance() throws SQLException {
        if (instance == null) {
            instance = new PackageItemsDTO();
        }

        return instance;
    }

    public ModelPackageItems getByID(Integer id) throws SQLException {
        return (ModelPackageItems) dao.queryForId(id);
    }
    
    public List<ModelPackageItems> getByPackageID(Integer packageID) throws SQLException {
        return dao.queryForEq("package_id", packageID);
    }

    public ModelPackageItems getByItemID(Integer itemID) throws SQLException {
        try {
            return (ModelPackageItems) dao.queryForEq("item_id", itemID).get(0);
        } catch (IndexOutOfBoundsException outOfBoundsException){
            return null;
        }
    }
}
