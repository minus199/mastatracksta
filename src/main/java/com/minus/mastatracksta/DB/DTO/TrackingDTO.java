package com.minus.mastatracksta.DB.DTO;

import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.BaseModel;
import com.minus.mastatracksta.DB.Models.ModelTracking;
import java.sql.SQLException;

/**
 * @author minus
 */
public class TrackingDTO extends BaseDTO {

    private static TrackingDTO instance;

    private TrackingDTO() throws SQLException {
        dao = DtoFactory.getInstance().getTrackingDao();
    }

    public static TrackingDTO getInstance() throws SQLException {
        if (instance == null) {
            instance = new TrackingDTO();
        }

        return instance;
    }

    public ModelTracking getByTrackingNumber(String trackingNumber) throws SQLException {
        BaseModel firstInList = getFirstInList(dao.queryForEq("trackingNumber", trackingNumber));
        return (ModelTracking) firstInList;
    }
    
    public ModelTracking getByUniqueKey(String key) throws SQLException{
        return (ModelTracking) getFirstInList(dao.queryForEq("uniqueToken", dao));
    }
}
