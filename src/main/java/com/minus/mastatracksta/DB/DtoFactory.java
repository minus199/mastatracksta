package com.minus.mastatracksta.DB;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.minus.mastatracksta.DB.Models.ModelAftershipException;
import com.minus.mastatracksta.DB.Models.ModelCheckpoint;
import com.minus.mastatracksta.DB.Models.ModelCourier;
import com.minus.mastatracksta.DB.Models.ModelImage;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.DB.Models.ModelItemMeta;
import com.minus.mastatracksta.DB.Models.ModelItemPicture;
import com.minus.mastatracksta.DB.Models.ModelPackage;
import com.minus.mastatracksta.DB.Models.ModelPackageItems;
import com.minus.mastatracksta.DB.Models.ModelRawEmails;
import com.minus.mastatracksta.DB.Models.ModelTracking;
import java.sql.SQLException;

/**
 * @author minus
 */
public class DtoFactory {

    private static DtoFactory instance;
    private ConnectionSource connectionSource;
    
    private Dao<ModelItem, Integer> itemDao;
    private Dao<ModelItemPicture, Integer> itemPicturesDao;
    private Dao<ModelPackage, Integer> packagesDao;
    private Dao<ModelPackageItems, Integer> packageItemsDao;
    private Dao<ModelItemMeta, Integer> itemMetaDao;
    private Dao<ModelImage, Integer> imageDao;
    private Dao<ModelCourier, Integer> courierDao;
    private Dao<ModelTracking, Integer> trackingDao;
    private Dao<ModelCheckpoint, Integer> checkpointDao;
    private Dao<ModelAftershipException, Integer> aftershipExceptionDao;
    private Dao<ModelRawEmails, Integer> rawEmailsDao;

    public void setConnectionSource(ConnectionSource source) throws SQLException, ClassNotFoundException {
        if (connectionSource != null)
            return; // overwritten value is not allowed once set
        
        connectionSource = source;
    }

    public static DtoFactory getInstance() throws SQLException {
        if (instance == null) {
            instance = new DtoFactory();
        }

        return instance;
    }

    public Dao<ModelItem, Integer> getItemDao() throws SQLException {
        if (itemDao == null) {
            itemDao = DaoManager.createDao(connectionSource, ModelItem.class);
        }

        return itemDao;
    }

    public Dao<ModelItemPicture, Integer> getItemPicturesDao() throws SQLException {
        if (itemPicturesDao == null) {
            itemPicturesDao = DaoManager.createDao(connectionSource, ModelItemPicture.class);
        }

        return itemPicturesDao;
    }

    public Dao<ModelPackage, Integer> getPackagesDao() throws SQLException {
        if (packagesDao == null) {
            packagesDao = DaoManager.createDao(connectionSource, ModelPackage.class);
        }

        return packagesDao;
    }

    public Dao<ModelPackageItems, Integer> getPackageItemsDao() throws SQLException {
        if (packageItemsDao == null) {
            packageItemsDao = DaoManager.createDao(connectionSource, ModelPackageItems.class);
        }

        return packageItemsDao;
    }
    
    public Dao<ModelItemMeta, Integer> getItemMetaDao() throws SQLException {
        if (itemMetaDao == null) {
            itemMetaDao = DaoManager.createDao(connectionSource, ModelItemMeta.class);
        }
        
        return itemMetaDao;
    }

    public Dao<ModelImage, Integer> getImageDao() throws SQLException {
        if (imageDao == null) {
            imageDao = DaoManager.createDao(connectionSource, ModelImage.class);
        }
        
        return imageDao;
    }
    
    public Dao<ModelCourier, Integer> getCouriorDao() throws SQLException{
        if (courierDao == null) {
            courierDao = DaoManager.createDao(connectionSource, ModelCourier.class);
        }
        
        return courierDao;
    }
    
    public Dao<ModelTracking, Integer> getTrackingDao() throws SQLException{
        if (trackingDao == null) {
            trackingDao = DaoManager.createDao(connectionSource, ModelTracking.class);
        }
        
        return trackingDao;
    }
    
    public Dao<ModelCheckpoint, Integer> getCheckpointDao() throws SQLException{
        if (checkpointDao == null) {
            checkpointDao = DaoManager.createDao(connectionSource, ModelCheckpoint.class);
        }
        
        return checkpointDao;
    }
    
    public Dao<ModelAftershipException, Integer> getAftershipExceptionDao() throws SQLException{
        if (aftershipExceptionDao == null) {
            aftershipExceptionDao = DaoManager.createDao(connectionSource, ModelAftershipException.class);
        }
        
        return aftershipExceptionDao;
    }
    
    public Dao<ModelRawEmails, Integer> getRawEmailsDao() throws SQLException{
        if (rawEmailsDao == null)
            rawEmailsDao = DaoManager.createDao(connectionSource, ModelRawEmails.class);
        
        return rawEmailsDao;
    }
}


