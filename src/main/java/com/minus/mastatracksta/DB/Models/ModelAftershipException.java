/*
 * Copyright 2015 Your Organisation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.minus.mastatracksta.DB.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author asafb
 */
@DatabaseTable(tableName = "aftershipException")
public class ModelAftershipException extends BaseModel {

    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField(dataType = DataType.DOUBLE_OBJ)
    private Double httpCode;

    @DatabaseField(dataType = DataType.DOUBLE_OBJ)
    private Double code;

    @DatabaseField
    private String type;

    @DatabaseField
    private String message;

    public ModelAftershipException() {
    }

    public ModelAftershipException(Double httpCode, Double code, String type, String message) {
        this.httpCode = httpCode;
        this.code = code;
        this.type = type;
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Double httpCode) {
        this.httpCode = httpCode;
    }

    public Double getCode() {
        return code;
    }

    public void setCode(Double code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
