package com.minus.mastatracksta.DB.Models;

import Enums.ISO3Country;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author minus
 */
@DatabaseTable(tableName = "checkpoint")
public class ModelCheckpoint extends BaseModel {

    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField(canBeNull = false, foreign = true, columnName = "tracking_id")
    private ModelTracking tracking;

    @DatabaseField(canBeNull = true)
    private String time;

    @DatabaseField(canBeNull = true)
    private String city;

    @DatabaseField(dataType = DataType.ENUM_INTEGER, canBeNull = true)
    private ISO3Country country;

    @DatabaseField
    private String createdAt;

    @DatabaseField
    private String message;

    @DatabaseField
    private String state;

    @DatabaseField
    private String tag;

    @DatabaseField(canBeNull = true)
    private String zipCode;

    public ModelCheckpoint() {
    }

    public enum Fields {
        id, tracking_id, time, city, country, createdAt, message, state, tag, zipCode;
    }

    public ModelCheckpoint(String createdAt, String message, String state, String tag, String zipCode) {
        this.createdAt = createdAt;
        this.message = message;
        this.state = state;
        this.tag = tag;
        this.zipCode = zipCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ISO3Country getCountry() {
        return country;
    }

    public void setCountry(ISO3Country country) {
        this.country = country;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public ModelTracking getTracking() {
        return tracking;
    }

    public void setTracking(ModelTracking tracking) {
        this.tracking = tracking;
    }
}
