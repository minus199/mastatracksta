package com.minus.mastatracksta.DB.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.net.URL;

/**
 * @author minus
 */
@DatabaseTable(tableName = "courier")
public class ModelCourier {

    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField(columnName = "slug", dataType = DataType.STRING)
    private String slug;

    @DatabaseField(columnName = "name", dataType = DataType.STRING)
    private String name;

    @DatabaseField(columnName = "other_name", dataType = DataType.STRING)
    private String otherName;

    @DatabaseField(columnName = "phone", dataType = DataType.STRING)
    private String phone;

    @DatabaseField(columnName = "web_url", dataType = DataType.SERIALIZABLE)
    private URL webURL;

    //List<String> requiredfields
    public ModelCourier() {
    }

    public ModelCourier(String slug, String name) {
        this.slug = slug;
        this.name = name;
    }

    public ModelCourier(String slug, String name, String otherName, String phone, URL webURL) {
        this.slug = slug;
        this.name = name;
        this.otherName = otherName;
        this.phone = phone;
        this.webURL = webURL;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public URL getWebURL() {
        return webURL;
    }

    public void setWebURL(URL webURL) {
        this.webURL = webURL;
    }
}
