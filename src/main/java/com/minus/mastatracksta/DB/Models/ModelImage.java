package com.minus.mastatracksta.DB.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;

/**
 * @author minus
 */
@DatabaseTable(tableName = "image")
public class ModelImage extends BaseModel{

    @DatabaseField(generatedId = true, canBeNull = false)
    private Integer id;

    @DatabaseField(dataType = DataType.STRING)
    private String imageName;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    byte[] imageBytes;

    @DatabaseField(dataType = DataType.INTEGER_OBJ)
    private Integer width;

    @DatabaseField(dataType = DataType.INTEGER_OBJ)
    private Integer height;

    @DatabaseField(dataType = DataType.DATE_STRING)
    private Date dateCreated;

    public ModelImage() {
    }

    public ModelImage(String imageName, byte[] imageBytes) {
        this.imageName = imageName;
        this.imageBytes = imageBytes;
    }
    
    public ModelImage(String imageName, byte[] imageBytes, Integer width, Integer height) {
        this.imageName = imageName;
        this.imageBytes = imageBytes;
        this.width = width;
        this.height = height;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }
}
