package com.minus.mastatracksta.DB.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.minus.mastatracksta.Modules.ENUMS.CurrencyEnum;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import java.net.URL;
import java.util.Date;

/**
 *
 * @author asafb
 */

@DatabaseTable(tableName = "item")
public class ModelItem extends BaseModel{
    @DatabaseField(generatedId = true)
    private Integer id;
    
    @DatabaseField
    private String title;
    
    @DatabaseField(dataType = DataType.LONG_STRING)
    private String description;
    
    @DatabaseField(index = true)
    private String itemCode;
    
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private URL itemURL;
    
    @DatabaseField
    private String location;
    
    /*
    @DatabaseField(dataType = DataType.FLOAT_OBJ)
    private Float shippingCost;
    
    @DatabaseField(dataType = DataType.ENUM_STRING)
    private Enum<CurrencyEnum> shippingCostCurrency;
    */
    
    @DatabaseField(dataType = DataType.FLOAT_OBJ)
    private Float price;
    
    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private CurrencyEnum priceCurrency;
    
    @DatabaseField(dataType = DataType.DATE_STRING)
    private Date dateCreated;

    public ModelItem(){}

    /**
     * Full constructor
     * @param title
     * @param description
     * @param itemCode
     * @param origin
     * @param itemURL
     * @param location
     * @param shippingCost
     * @param shippingCostCurrency
     * @param price
     * @param priceCurrency 
     */
    public ModelItem(String title, String description, String itemCode, Enum<OriginEnum> origin, URL itemURL, String location, Float price, CurrencyEnum priceCurrency) {
        this.title = title;
        this.description = description;
        this.itemCode = itemCode;
        this.itemURL = itemURL;
        this.location = location;
        /*this.shippingCost = shippingCost;
        this.shippingCostCurrency = shippingCostCurrency;*/
        this.price = price;
        this.priceCurrency = priceCurrency;
    }

    /**
     * Minimal constructor
     * @param title
     * @param itemCode
     * @param itemURL 
     */
    public ModelItem(String title, String itemCode, URL itemURL) {
        this.title = title;
        this.itemCode = itemCode;
        this.itemURL = itemURL;
        
        this.description = "N/A";
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public URL getItemURL() {
        return itemURL;
    }

    public void setItemURL(URL itemURL) {
        this.itemURL = itemURL;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
/*
    public Float getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(Float shippingCost) {
        this.shippingCost = shippingCost;
    }

    public Enum<CurrencyEnum> getShippingCostCurrency() {
        return shippingCostCurrency;
    }

    public void setShippingCostCurrency(Enum<CurrencyEnum> shippingCostCurrency) {
        this.shippingCostCurrency = shippingCostCurrency;
    }
*/
    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Enum<CurrencyEnum> getPriceCurrency() {
        return priceCurrency;
    }
    
    public void setPriceCurrency(CurrencyEnum priceCurrency) {
        this.priceCurrency = priceCurrency;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
