package com.minus.mastatracksta.DB.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.minus.mastatracksta.Modules.ENUMS.BaseMetaEnum;
import com.minus.mastatracksta.Modules.ENUMS.EBayItemMetaEnum;
import java.util.Date;
import java.util.HashMap;

/**
 * @author minus
 */

@DatabaseTable(tableName = "itemMeta")
public class ModelItemMeta extends BaseModel{
    @DatabaseField(generatedId = true)
    private Integer id;
    
    @DatabaseField()
    private Integer itemID;
    
    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private EBayItemMetaEnum metaKey;
    
    @DatabaseField(dataType = DataType.LONG_STRING)
    private String metaValue;

    @DatabaseField(dataType = DataType.DATE_STRING)
    private Date dateCreated;

    public ModelItemMeta() {
    }

    public ModelItemMeta(Integer itemID, EBayItemMetaEnum metaKey, String metaValue) {
        this.itemID = itemID;
        this.metaKey = metaKey;
        this.metaValue = metaValue;
        this.dateCreated = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public EBayItemMetaEnum getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(EBayItemMetaEnum metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
