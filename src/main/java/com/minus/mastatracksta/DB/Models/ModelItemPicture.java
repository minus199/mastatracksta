package com.minus.mastatracksta.DB.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.net.URL;

/**
 * @author minus
 */
@DatabaseTable(tableName = "itemPictures")
public class ModelItemPicture extends BaseModel{

    @DatabaseField(generatedId = true, canBeNull = false)
    private Integer id;

    @DatabaseField(canBeNull = false, foreign = true, columnName = "item_id")
    private ModelItem item;
    
    @DatabaseField(canBeNull = false, foreign = true, columnName = "image_id")
    private ModelImage modelImage;
    
    @DatabaseField(dataType = DataType.SERIALIZABLE, canBeNull = true)
    private URL pictureURL;
    
    public ModelItemPicture() {
    }

    public ModelItemPicture(ModelItem item, ModelImage image) {
        this.item = item;
        this.modelImage = image;
    }
    
    public ModelItemPicture(ModelItem item, ModelImage image, URL pictureSourceURL) {
        this.item = item;
        this.modelImage = image;
        pictureURL = pictureSourceURL;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ModelItem getItem() {
        return item;
    }

    public void setItem(ModelItem item) {
        this.item = item;
    }

    public ModelImage getModelImage() {
        return modelImage;
    }

    public void setModelImage(ModelImage modelImage) {
        this.modelImage = modelImage;
    }

    public URL getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(URL pictureURL) {
        this.pictureURL = pictureURL;
    }
}
