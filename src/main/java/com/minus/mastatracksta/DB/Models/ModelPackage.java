/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minus.mastatracksta.DB.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import java.util.Date;

/**
 *
 * @author asafb
 */
@DatabaseTable(tableName = "package")
public class ModelPackage extends BaseModel {

    @DatabaseField(generatedId = true, canBeNull = false)
    private Integer id;

    @DatabaseField(dataType = DataType.ENUM_STRING)
    private OriginEnum origin;

    @DatabaseField(useGetSet = true)
    private String trackingNumber;

    @DatabaseField(dataType = DataType.BOOLEAN_OBJ, useGetSet = true)
    private Boolean isActive;

    @DatabaseField(dataType = DataType.BOOLEAN_OBJ, useGetSet = true)
    private Boolean isArchived;

    @DatabaseField(dataType = DataType.BOOLEAN_OBJ, useGetSet = true)
    private Boolean isDeleted;

    @DatabaseField(dataType = DataType.DATE)
    private Date dateCreated;

    public ModelPackage() {
    }

    public ModelPackage(String trackingNumber) {
        this.dateCreated = new Date();
        this.isActive = true;
        this.isArchived = true;
        this.isDeleted = true;
        this.trackingNumber = trackingNumber;
    }

    public Integer getId() {
        return id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsArchived() {
        return isArchived;
    }

    public void setIsArchived(Boolean isArchived) {
        this.isArchived = isArchived;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public OriginEnum getOrigin() {
        return origin;
    }

    public void setOrigin(OriginEnum origin) {
        this.origin = origin;
    }
}
