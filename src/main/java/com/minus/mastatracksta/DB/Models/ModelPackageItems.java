package com.minus.mastatracksta.DB.Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author asafb
 */

@DatabaseTable(tableName = "packageItems")
public class ModelPackageItems extends BaseModel{
    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField(canBeNull = false, foreign = true, columnName = "package_id")
    private ModelPackage pkg;

    @DatabaseField(canBeNull = false, foreign = true, columnName = "item_id")
    private ModelItem item;

    public ModelPackageItems() {}

    public ModelPackageItems(ModelPackage pkg, ModelItem item) {
        this.pkg = pkg;
        this.item = item;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ModelPackage getPkg() {
        return pkg;
    }

    public void setPkg(ModelPackage pkg) {
        this.pkg = pkg;
    }

    public ModelItem getItem() {
        return item;
    }

    public void setItem(ModelItem item) {
        this.item = item;
    }
}
