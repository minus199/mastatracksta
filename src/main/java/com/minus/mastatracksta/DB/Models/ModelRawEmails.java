package com.minus.mastatracksta.DB.Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author minus
 */
@DatabaseTable(tableName = "rawEMails")
public class ModelRawEmails extends BaseModel{
    @DatabaseField(generatedId = true)
    private Integer id;
    @DatabaseField(columnName = "history_id")
    private BigInteger historyID;
    @DatabaseField(columnName = "internal_date")
    private Long internalDate;
    @DatabaseField(columnName = "payload")
    private String payload;
    @DatabaseField(columnName = "snippet")
    private String snippet;
    @DatabaseField(columnName = "message_decoded_content")
    private String messageDecodedContent;

    public ModelRawEmails() {
    }

    public ModelRawEmails(BigInteger historyID, Long internalDate, String payload, String snippet, String messageDecodedContent) {
        this.historyID = historyID;
        this.internalDate = internalDate;
        this.payload = payload;
        this.snippet = snippet;
        this.messageDecodedContent = messageDecodedContent;
    }

    public ModelRawEmails(String messageDecodedContent) {
        this.messageDecodedContent = messageDecodedContent;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigInteger getHistoryID() {
        return historyID;
    }

    public void setHistoryID(BigInteger historyID) {
        this.historyID = historyID;
    }

    public Long getInternalDate() {
        return internalDate;
    }

    public void setInternalDate(Long internalDate) {
        this.internalDate = internalDate;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getMessageDecodedContent() {
        return messageDecodedContent;
    }

    public void setMessageDecodedContent(String messageDecodedContent) {
        this.messageDecodedContent = messageDecodedContent;
    }
}
