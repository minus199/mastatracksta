package com.minus.mastatracksta.DB.Models;

import Enums.ISO3Country;
import Enums.StatusTag;
import com.google.gson.Gson;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;
import java.util.List;

/**
 * @author minus
 */
@DatabaseTable(tableName = "tracking")
public class ModelTracking extends BaseModel {

    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField
    private String slug;

    @DatabaseField(uniqueIndex = true, indexName = "trackingNumber")
    private String trackingNumber;

    @DatabaseField(dataType = DataType.INTEGER)
    int trackedCount;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private StatusTag tag;

    @DatabaseField
    private String orderID;

    @DatabaseField
    private String orderIdPath;

    @DatabaseField
    private String customerName;

    @DatabaseField(dataType = DataType.LONG_STRING)
    String emails;
    
    @DatabaseField
    private String shipmentType;

    @DatabaseField
    private String signedBy;

    @DatabaseField
    private String postalCode;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    ISO3Country originCountry;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    ISO3Country destinationCountry;

    @DatabaseField(dataType = DataType.INTEGER)
    int shipmentPackageCount;

    @DatabaseField
    private String title;

    @DatabaseField
    private String source;

    @DatabaseField
    private String trackingAccountNumber;

    @DatabaseField(uniqueIndex = true)
    private String uniqueToken;

    @DatabaseField
    private String expectedDelivery;

    @DatabaseField
    private String trackingShipDate;

    @DatabaseField(dataType = DataType.INTEGER)
    int deliveryDate;

    @DatabaseField(dataType = DataType.DATE_STRING)
    Date createdAt;

    @DatabaseField(dataType = DataType.DATE_STRING)
    Date updatedAt;

    public ModelTracking() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public int getTrackedCount() {
        return trackedCount;
    }

    public void setTrackedCount(int trackedCount) {
        this.trackedCount = trackedCount;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOrderIdPath() {
        return orderIdPath;
    }

    public void setOrderIdPath(String orderIdPath) {
        this.orderIdPath = orderIdPath;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getSignedBy() {
        return signedBy;
    }

    public void setSignedBy(String signedBy) {
        this.signedBy = signedBy;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public ISO3Country getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(ISO3Country originCountry) {
        this.originCountry = originCountry;
    }

    public ISO3Country getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(ISO3Country destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public int getShipmentPackageCount() {
        return shipmentPackageCount;
    }

    public void setShipmentPackageCount(int shipmentPackageCount) {
        this.shipmentPackageCount = shipmentPackageCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTrackingAccountNumber() {
        return trackingAccountNumber;
    }

    public void setTrackingAccountNumber(String trackingAccountNumber) {
        this.trackingAccountNumber = trackingAccountNumber;
    }

    public String getUniqueToken() {
        return uniqueToken;
    }

    public void setUniqueToken(String uniqueToken) {
        this.uniqueToken = uniqueToken;
    }

    public String getExpectedDelivery() {
        return expectedDelivery;
    }

    public void setExpectedDelivery(String expectedDelivery) {
        this.expectedDelivery = expectedDelivery;
    }

    public String getTrackingShipDate() {
        return trackingShipDate;
    }

    public void setTrackingShipDate(String trackingShipDate) {
        this.trackingShipDate = trackingShipDate;
    }

    public int getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(int deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public StatusTag getTag() {
        return tag;
    }

    public void setTag(StatusTag tag) {
        this.tag = tag;
    }

    public List<String> getEmails() {
        return new Gson().fromJson(emails, List.class);
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }
}
