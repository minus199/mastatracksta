package com.minus.mastatracksta.DB;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;

/**
 *
 * @author minus
 */
public class TableCreator {
    final public static void createAllIfNotExists(ConnectionSource connectionSource) throws SQLException {
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelPackage.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelItem.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelItemPicture.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelPackageItems.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelItemMeta.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelImage.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelCourier.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelTracking.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelCheckpoint.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelAftershipException.class);
        TableUtils.createTableIfNotExists(connectionSource, com.minus.mastatracksta.DB.Models.ModelRawEmails.class);
    }
}
