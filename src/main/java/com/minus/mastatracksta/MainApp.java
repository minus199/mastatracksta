package com.minus.mastatracksta;

import Utils.MsgGen;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MainApp extends Application {

    public static final String APP_NAME = "MastaTracksta";

    private Scene scene;
    private Stage stage;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            initView(primaryStage);
        } catch (Exception ex) {
            MsgGen.alertException(ex);
            System.exit(1);
        }
    }

    public void initView(Stage primaryStage) throws IOException {
        this.stage = primaryStage;
        GridPane root = FXMLLoader.load(getClass().getResource("/fxml/mainView.fxml"));
        scene = new Scene(root);
        prepareStage().loadCss();
    }

    private MainApp prepareStage() {
        stage.setTitle(APP_NAME);
        stage.setScene(scene);
        positionAndSizeStage();

        stage.show();

        attachStageEventListeners(Boolean.TRUE);

        return this;
    }

    private void positionAndSizeStage() {
        stage.setMaxHeight(700);

        Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
        double width = bounds.getWidth() * 0.85;
        double height = bounds.getHeight();

        stage.setMinWidth(width);
        stage.setMaxWidth(width);
        stage.setMinHeight(height * 0.5);
        stage.setMaxHeight(height * 0.7);

        stage.centerOnScreen();
        stage.setX(width * 0.1);
        stage.setY(height * 0.1);
    }

    private MainApp attachStageEventListeners(Boolean debug) {
        stage.setOnCloseRequest((WindowEvent event) -> {
            Platform.exit();
            System.exit(0);
        });

        stage.iconifiedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
            System.out.println("minimized:" + t1.booleanValue());
        });

        stage.maximizedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
            System.out.println("maximized:" + t1.booleanValue());
        });

        stage.titleProperty().bind(
                scene.xProperty().asString().
                concat(" : ").
                concat(scene.yProperty().asString()));

        return this;
    }

    private MainApp loadCss() {
        URL cssRsc = getClass().getResource("/styles/MainApp.css");
        scene.getStylesheets().clear();
        scene.getStylesheets().add(cssRsc.toString());
        /*scene.setOnDragEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                // for paste as file, e.g. in Windows Explorer
                try {

                    Clipboard clipboard = Clipboard.getSystemClipboard();
                    Dragboard db = scene.startDragAndDrop(TransferMode.ANY);

                    ClipboardContent content = new ClipboardContent();

                    Image sourceImage = scene.getImage();

                    Object userData = scene.getUserData();

                    String name = FilenameUtils.getBaseName(imageInfo.getName());
                    String ext = FilenameUtils.getExtension(imageInfo.getName());

                    ///Avoid get "prefix lenght too short" error when file name lenght <= 3
                    if (name.length() < 4) {
                        name = name + Long.toHexString(Double.doubleToLongBits(Math.random()));;
                    }

                    File temp = File.createTempFile(name, "." + ext);

                    if (ext.contentEquals("jpg") || ext.contentEquals("jpeg")) {
                        BufferedImage image = SwingFXUtils.fromFXImage(sourceImage, null); // Get buffered image.
                        BufferedImage imageRGB = new BufferedImage(image.getWidth(), image.getHeight(),
                                BufferedImage.OPAQUE);
                        Graphics2D graphics = imageRGB.createGraphics();
                        graphics.drawImage(image, 0, 0, null);
                        ImageIO.write(imageRGB, ext, temp);
                        graphics.dispose();

                        ImageIO.write(imageRGB,
                                ext, temp);

                    } else {

                        ImageIO.write(SwingFXUtils.fromFXImage(sourceImage, null),
                                ext, temp);
                    }

                    content.putFiles(java.util.Collections.singletonList(temp));
                    db.setContent(content);
                    clipboard.setContent(content);
                    event.consume();

                    temp.deleteOnExit();

                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }

            }
        });*/
        return this;
    }

    public Scene getScene() {
        return scene;
    }

    public Stage getStage() {
        return stage;
    }

    public static class MetaInfoModel {

        private final String metaName;
        private final Object metaValue;

        public MetaInfoModel(String metaName, Object metaValue) {
            this.metaName = metaName;
            this.metaValue = metaValue;
        }

        public String getMetaName() {
            return metaName;
        }

        public Object getMetaValue() {
            return metaValue;
        }
    }
}
