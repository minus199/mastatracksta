/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minus.mastatracksta;

import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.Modules.PictureGallery.Slideshow;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import com.minus.mastatracksta.ShippingPackage.Item.resolvers.ByIdItemResolver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author asafb
 */
public class MenuUtils {
    public static ChoiceBox initOriginList(){
        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(OriginEnum.values()));
        
        cb.getSelectionModel().selectedIndexProperty().addListener(new
            ChangeListener<Number>() {
                public void changed(ObservableValue ov,
                    Number value, Number new_value) {
                    //label.setText(greetings[new_value.intValue()]);
            }
        });

        cb.setTooltip(new Tooltip("Select item origin"));
        cb.setValue(OriginEnum.UNSPECIFIED);
        
        return cb;
    }
    
    public static ListView createPkgList(ListView pkgItemListContainer){
        ArrayList<String> trackingNumbers = new ArrayList<String>();

        for (int i = 0; i < 100; i++) {
            trackingNumbers.add("RS" + 1000 + i + "85556NL");
        }

        pkgItemListContainer.setItems(FXCollections.observableArrayList(trackingNumbers));
        return pkgItemListContainer;
    }
    
    public static ChoiceBox initCountryList(){
        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(Locale.getISOCountries()));
        
        cb.getSelectionModel().selectedIndexProperty().addListener(new
            ChangeListener<Number>() {
                public void changed(ObservableValue ov,
                    Number value, Number new_value) {
                    //label.setText(greetings[new_value.intValue()]);
            }
        });

        cb.setTooltip(new Tooltip("Select country"));
        cb.setValue("US");
        
        //((AnchorPane) getRoot()).getChildren().add(cb);
        return cb;
    }
    
    private static ImageView createImageView(final File imageFile) {

        // DEFAULT_THUMBNAIL_WIDTH is a constant you need to define
        // The last two arguments are: preserveRatio, and use smooth (slower)
        // resizing
        ImageView imageView = null;
        try {
            final Image image = new Image(new FileInputStream(imageFile), 150, 0, true,
                    true);
            imageView = new ImageView(image);
            imageView.setFitWidth(150);
            imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent mouseEvent) {
                    if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                        if (mouseEvent.getClickCount() == 2) {
                            try {
                                BorderPane borderPane = new BorderPane();
                                ImageView imageView = new ImageView();
                                Image image = new Image(new FileInputStream(imageFile));
                                imageView.setImage(image);
                                imageView.setStyle("-fx-background-color: BLACK");
                                //imageView.setFitHeight(primaryStage.getHeight() - 10);
                                imageView.setPreserveRatio(true);
                                imageView.setSmooth(true);
                                imageView.setCache(true);
                                borderPane.setCenter(imageView);
                                borderPane.setStyle("-fx-background-color: BLACK");
                                Stage newStage = new Stage();
                                //newStage.setWidth(primaryStage.getWidth());
                                //newStage.setHeight(primaryStage.getHeight());
                                newStage.setTitle(imageFile.getName());
                                Scene scene = new Scene(borderPane, Color.BLACK);
                                newStage.setScene(scene);
                                newStage.show();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return imageView;
    }
}
