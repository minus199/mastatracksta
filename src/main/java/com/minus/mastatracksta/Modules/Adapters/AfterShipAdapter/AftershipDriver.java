package com.minus.mastatracksta.Modules.Adapters.AfterShipAdapter;

import Classes.AftershipAPIException;
import Classes.ConnectionAPI;
import Classes.Courier;
import Classes.Tracking;
import Exceptions.trackingDriver.UnableToConnectException;
import Exceptions.trackingDriver.afterShip.MoreThanOneCourierDetectedException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.minus.mastatracksta.DB.DTO.AftershipExceptionDTO;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelAftershipException;
import com.minus.mastatracksta.DB.Models.ModelCourier;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author minus
 */
public class AftershipDriver extends TrackingDriverBase {
    public static final String API_KEY = "74f3e2f4-b677-470c-9550-ccbea010d149";
    
    private List<Courier> fetchedCouriers;
    
    @Override
    public ConnectionAPI getConnection() throws UnableToConnectException{
        try {
            if (AftershipExceptionDTO.getInstance().isEmpty())
                populateExceptions();
        } catch (SQLException ex) {
            throw new UnableToConnectException(ex);
        }
        
        return new ConnectionAPI(API_KEY);
    }

    @Override
    final public List<Courier> autoDetectCourier(String trackingNumber) throws AftershipAPIException, IOException, ParseException, UnableToConnectException, MoreThanOneCourierDetectedException {
        List<Courier> detectCouriers = getConnection().detectCouriers(trackingNumber);

        return detectCouriers;
    }

    @Override
    public Tracking addTracking(String trackingNumber) throws AftershipAPIException, IOException, ParseException, UnableToConnectException {
        Tracking tracking = new Tracking(trackingNumber);

        //Then we can add information;
        tracking.setSlug("dpd");
        tracking.setTitle("this title");
        tracking.addEmails("email@yourdomain.com");
        tracking.addEmails("another_email@yourdomain.com");
        tracking.addSmses("+85292345678");
        tracking.addSmses("+85292345679");

        //Even add customer fields
        tracking.addCustomFields("product_name", "iPhone Case");
        tracking.addCustomFields("product_price", "USD19.99");

        //Finally we add the tracking to our account
        return getConnection().postTracking(tracking);
    }

    @Override
    protected ArrayList<ModelCourier> _getCouriers() throws AftershipAPIException, MalformedURLException, SQLException, IOException, ParseException, UnableToConnectException {
        fetchedCouriers = getConnection().getAllCouriers();
        for (Courier courier : fetchedCouriers) {
            URL courierURL;
            try {
                courierURL = new URL(courier.getWeb_url());
            } catch (MalformedURLException exception) {
                courierURL = new URL("http://" + courier.getWeb_url());
            }

            persistCourier(courier.getSlug(), courier.getName(), courier.getOther_name(), courier.getPhone(), courierURL);
        }

        return this.modelCouriers;
    }

    private static void populateExceptions() throws SQLException {
        String exceptionsListJson;
        exceptionsListJson = "[{\"httpCode\":\"400\",\"code\":\"400\",\"type\":\"BadRequest\",\"message\":\"The request was invalid or cannot be otherwise served.\"},{\"httpCode\":\"400\",\"code\":\"4001\",\"type\":\"BadRequest\",\"message\":\"Invalid JSON data.\"},{\"httpCode\":\"400\",\"code\":\"4002\",\"type\":\"BadRequest\",\"message\":\"Invalid JSON data.\"},{\"httpCode\":\"400\",\"code\":\"4003\",\"type\":\"BadRequest\",\"message\":\"Tracking already exists.\"},{\"httpCode\":\"400\",\"code\":\"4004\",\"type\":\"BadRequest\",\"message\":\"Tracking does not exist.\"},{\"httpCode\":\"400\",\"code\":\"4005\",\"type\":\"BadRequest\",\"message\":\"The value of `tracking_number` is invalid.\"},{\"httpCode\":\"400\",\"code\":\"4006\",\"type\":\"BadRequest\",\"message\":\"`tracking` object is required.\"},{\"httpCode\":\"400\",\"code\":\"4007\",\"type\":\"BadRequest\",\"message\":\"`tracking_number` is required.\"},{\"httpCode\":\"400\",\"code\":\"4008\",\"type\":\"BadRequest\",\"message\":\"The value of `[field_name]` is invalid.\"},{\"httpCode\":\"400\",\"code\":\"4009\",\"type\":\"BadRequest\",\"message\":\"`[field_name]` is required.\"},{\"httpCode\":\"400\",\"code\":\"4010\",\"type\":\"BadRequest\",\"message\":\"The value of `slug` is invalid.\"},{\"httpCode\":\"400\",\"code\":\"4011\",\"type\":\"BadRequest\",\"message\":\"Missing or invalid value of the required fields for this courier. Besides `tracking_number`, also required: `[field_name]`\"},{\"httpCode\":\"400\",\"code\":\"4012\",\"type\":\"BadRequest\",\"message\":\"Cannot detect courier. Activate courier at https://www.aftership.com/settings/courier\"},{\"httpCode\":\"400\",\"code\":\"4013\",\"type\":\"BadRequest\",\"message\":\"Retrack is not allowed. You can only retrack an inactive tracking.\"},{\"httpCode\":\"400\",\"code\":\"4014\",\"type\":\"BadRequest\",\"message\":\"`notification` object is required.\"},{\"httpCode\":\"400\",\"code\":\"4015\",\"type\":\"BadRequest\",\"message\":\"The value of `id` is invalid.\"},{\"httpCode\":\"400\",\"code\":\"4016\",\"type\":\"BadRequest\",\"message\":\"Retrack is not allowed. You can only retrack each shipment once.\"},{\"httpCode\":\"400\",\"code\":\"4017\",\"type\":\"BadRequest\",\"message\":\"The format of `tracking_number` is invalid.\"},{\"httpCode\":\"401\",\"code\":\"401\",\"type\":\"Unauthorized\",\"message\":\"Invalid API key.\"},{\"httpCode\":\"403\",\"code\":\"403\",\"type\":\"Forbidden\",\"message\":\"The request is understood, but it has been refused or access is not allowed.\"},{\"httpCode\":\"404\",\"code\":\"404\",\"type\":\"NotFound\",\"message\":\"The URI requested is invalid or the resource requested does not exist.\"},{\"httpCode\":\"429\",\"code\":\"429\",\"type\":\"TooManyRequests\",\"message\":\"You have exceeded the API call rate limit. Default limit is 10 requests per second.\"},{\"httpCode\":\"500\",\"code\":\"500\",\"type\":\"InternalError\",\"message\":\"Something went wrong on AfterShip's end.\"},{\"httpCode\":\"502\",\"code\":\"502\",\"type\":\"InternalError\",\"message\":\"Something went wrong on AfterShip's end.\"},{\"httpCode\":\"503\",\"code\":\"503\",\"type\":\"InternalError\",\"message\":\"Something went wrong on AfterShip's end.\"},{\"httpCode\":\"504\",\"code\":\"504\",\"type\":\"InternalError\",\"message\":\"Something went wrong on AfterShip's end.\"}]";
        
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Double.class, (JsonDeserializer<Double>) (JsonElement je, Type type, JsonDeserializationContext jdc) -> {
            try {
                return je.getAsDouble();
            } catch (Exception ex) {
                return 0.0;
            }
        });
        
        Gson gson = builder.create();
        ModelAftershipException[] fromJson = gson.fromJson(exceptionsListJson, ModelAftershipException[].class);

        for (ModelAftershipException model : fromJson) {
            DtoFactory.getInstance().getAftershipExceptionDao().create(model);
        }
    }
}
