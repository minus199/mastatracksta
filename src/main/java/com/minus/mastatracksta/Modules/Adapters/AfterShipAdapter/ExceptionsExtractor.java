package com.minus.mastatracksta.Modules.Adapters.AfterShipAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import org.apache.commons.io.FileUtils;
import static org.hamcrest.CoreMatchers.is;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author minus
 */
public class ExceptionsExtractor {

    public static void test() throws IOException {
        File input = new File("/tmp/input.html");
        input.deleteOnExit();

        URL url = new URL("https://www.aftership.com/docs/api/4/errors");

        URLConnection yc = url.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                yc.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            System.out.println(inputLine);
        }
        in.close();

        /*org.jsoup.nodes.Document doc = Jsoup.parse(, 10000);

         Elements select = doc.select(".table-condensed tbody tr");
         for (Iterator<Element> iterator = select.iterator(); iterator.hasNext();) {
         Element next = iterator.next();
         Elements trChildren = next.getElementsByTag("td");

         Element httpStatusCode = trChildren.get(0);
         Element metaCode = trChildren.get(1);
         Element metaType = trChildren.get(2);
         Element errorMessage = trChildren.get(3);

         System.out.println(httpStatusCode.text());

         if (httpStatusCode.getElementsByTag("code").size() > 1) {

         }
         }*/
    }
}
