package com.minus.mastatracksta.Modules.Adapters.AfterShipAdapter;

import Classes.AftershipAPIException;
import Classes.ConnectionAPI;
import Classes.Courier;
import Classes.Tracking;
import Exceptions.trackingDriver.UnableToConnectException;
import com.minus.mastatracksta.DB.DTO.CourierDTO;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelCourier;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author minus
 */
public abstract class TrackingDriverBase {
    protected ArrayList<ModelCourier> modelCouriers = new ArrayList();

    public abstract ConnectionAPI getConnection() throws UnableToConnectException;

    public abstract List<Courier> autoDetectCourier(String trackingNumber) throws Exception;

    public abstract Tracking addTracking(String trackingNumber) throws Exception;

    protected abstract ArrayList<ModelCourier> _getCouriers() throws Exception;

    final public List<ModelCourier> getCouriers() throws IOException, ParseException, Exception {
        if (this.modelCouriers.size() > 1) {
            return this.modelCouriers;
        }

        if (!CourierDTO.getInstance().isEmpty()) {
            return this.modelCouriers = (ArrayList<ModelCourier>) CourierDTO.getInstance().getAll();
        }

        return _getCouriers();
    }

    final protected void persistCourier(String slug, String name, String otherName, String phone, URL courierURL) throws SQLException {
        ModelCourier modelCourier = new ModelCourier(slug, name, otherName, phone, courierURL);
        modelCourier.setId(DtoFactory.getInstance().getCouriorDao().create(modelCourier));
        this.modelCouriers.add(modelCourier);
    }
}
