package com.minus.mastatracksta.Modules.Adapters.AliExpressAdapter;

import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author minus
 */
public class AliExpressItem extends Item{
    public AliExpressItem(String itemCode) throws SQLException, IOException, Exception {
        super(itemCode);
    }
}
