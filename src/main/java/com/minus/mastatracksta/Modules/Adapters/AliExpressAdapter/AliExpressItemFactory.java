package com.minus.mastatracksta.Modules.Adapters.AliExpressAdapter;

import com.minus.mastatracksta.ShippingPackage.Descriptors.ItemMetaInfoDescriptor;
import com.minus.mastatracksta.ShippingPackage.Item.ItemFactory;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import java.net.URL;
import org.w3c.dom.NodeList;

/**
 * @author minus
 */
public class AliExpressItemFactory extends ItemFactory{

    public AliExpressItemFactory(String itemCode) {
        super(itemCode);
    }

    public AliExpressItemFactory(String itemCode, OriginEnum originEnum) {
        super(itemCode, originEnum);
    }

    public AliExpressItemFactory(String itemCode, URL itemURL) {
        super(itemCode, itemURL);
    }

    @Override
    protected AliExpressItemFactory extractMetaData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ItemFactory populate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
