package com.minus.mastatracksta.Modules.Adapters.AliExpressAdapter;

import Classes.AftershipAPIException;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;
import Exceptions.trackingDriver.afterShip.MoreThanOneCourierDetectedException;
import java.net.URL;
import java.sql.SQLException;

/**
 * @author minus
 */
public class AliExpressPackage extends com.minus.mastatracksta.ShippingPackage.Package{

    public AliExpressPackage(String trackingNumber) throws IllegalAccessException, InstantiationException, SQLException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, Exception {
        super(trackingNumber);
    }

    public AliExpressPackage(Integer pkgID) throws SQLException, AftershipAPIException, IllegalAccessException, InstantiationException, Exception {
        super(pkgID);
    }
    
}
