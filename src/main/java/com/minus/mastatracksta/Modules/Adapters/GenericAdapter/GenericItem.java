package com.minus.mastatracksta.Modules.Adapters.GenericAdapter;

import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author minus
 */
public class GenericItem extends Item{

    public GenericItem(String itemCode) throws SQLException, IOException, Exception {
        super(itemCode);
    }

}
