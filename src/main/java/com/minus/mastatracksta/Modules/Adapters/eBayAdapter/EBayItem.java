package com.minus.mastatracksta.Modules.Adapters.eBayAdapter;

import com.minus.mastatracksta.MainApp;
import com.minus.mastatracksta.Modules.ENUMS.EBayItemMetaEnum;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author minus
 */
public class EBayItem extends Item {

    private String primaryCategoryName;
    private URL naturalSearchURL;

    public EBayItem(String itemCode) throws SQLException, IOException, Exception {
        super(itemCode);
    }

    @Override
    public ArrayList<MainApp.MetaInfoModel> toMetaInfoModels() throws Exception {
        ArrayList<MainApp.MetaInfoModel> infoModels = super.toMetaInfoModels();
        infoModels.add(new MainApp.MetaInfoModel("categoryName", getPrimaryCategoryName()));
        infoModels.add(new MainApp.MetaInfoModel("urlForSearch", getNaturalSearchURL()));

        return infoModels;
    }

    public String getPrimaryCategoryName() throws SQLException {
        if (primaryCategoryName != null)
            return primaryCategoryName;
        
        return primaryCategoryName = getMeta().getOne(EBayItemMetaEnum.PRIMARY_CATEGORY_NAME);
    }

    public URL getNaturalSearchURL() throws SQLException, MalformedURLException {
        if (naturalSearchURL != null)
            return naturalSearchURL;
        
        String one = getMeta().getOne(EBayItemMetaEnum.URL_FOR_SEARCH);
        if (one == null) {
            return null;
        }

        return naturalSearchURL = new URL(one);
    }
}
