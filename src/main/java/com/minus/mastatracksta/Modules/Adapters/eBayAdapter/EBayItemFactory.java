package com.minus.mastatracksta.Modules.Adapters.eBayAdapter;

import Exceptions.UnableToPopulateItemException;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.ShippingPackage.Item.ItemFactory;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.Modules.ENUMS.EBayItemMetaEnum;
import com.minus.mastatracksta.ShippingPackage.Item.ItemMeta;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author minus
 */
final public class EBayItemFactory extends ItemFactory {

    final public static String eBayEntryPoint = "http://open.api.ebay.com/shopping";
    final public static String eBayName = "shipmentTracker";
    final public static String eBayDEVID = "13660224-238f-4886-814d-547d80633e92";
    final public static String eBayAppID = "AsafBlum-fc49-4c50-a341-2a9f75be9f6b";
    final public static String eBayCertID = "1522c88a-efd3-47fa-84ee-0286526d57d8";

    private HashMap processedItems = new HashMap();
    private List<NameValuePair> qParams;
    private URL urlForNaturalSearch;
    private String primaryCategoryName;
    private Element xmlResponse;

    public EBayItemFactory(String itemCode) {
        super(itemCode);
        qParams = new ArrayList<>();
        qParams = getqParams();
    }

    public EBayItemFactory(String itemCode, OriginEnum originEnum) {
        super(itemCode, originEnum);
        qParams = new ArrayList<>();
        qParams = getqParams();
    }

    public EBayItemFactory(String itemCode, URL itemURL) {
        super(itemCode, itemURL);
    }
    
    private EBayItemFactory doRequest() throws IOException, ParserConfigurationException {
        URL url;
        try {
            url = new URL(eBayEntryPoint + "?" + getEncodedUrlParams());
        } catch (MalformedURLException ex) {
            Logger.getLogger(EBayItemFactory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        StreamSource xmlSource = new StreamSource(url.openStream());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document xmlDocument;

        try {
            xmlDocument = db.parse(xmlSource.getInputStream());
        } catch (SAXException | IOException ex) {
            Logger.getLogger(EBayItemFactory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        xmlResponse = (Element) xmlDocument.getElementsByTagName("GetSingleItemResponse").item(0);
        return this;
    }
    
    private EBayItemFactory processResponse(){
        ArrayList metaItemNames = new ArrayList<>
        (Arrays.asList(
                "Title",
                "PrimaryCategoryName",
                "ItemSpecifics",
                "Description",
                "PictureURL",
                "Location",
                "Country",
                "ViewItemURLForNaturalSearch",
                "ShippingCostSummary",
                "ConvertedCurrentPrice"
        ));
        
        for (Iterator iterator = metaItemNames.iterator(); iterator.hasNext();) {
            String item = (String)iterator.next();
            processItem((String)item);
        }
        
        return this;
    }
    
    final protected EBayItemFactory build(){
        setTitle((String)processedItems.get("Title"));
        setDescription((String) processedItems.get("Description"));
        
        /* Can be one, can be more */
        try {
            setPictureURLs((ArrayList) processedItems.get("PictureURL"));
        } catch (ClassCastException e) {
            setPictureURLs((String) processedItems.get("PictureURL"));
        }
        
        setLocationDescriptor((String) processedItems.get("Location"), (String) processedItems.get("Country"));
        setPriceDescriptor((String) processedItems.get("ConvertedCurrentPrice"), "ConvertedCurrentPrice:currencyID");

        primaryCategoryName = (String) processedItems.get("PrimaryCategoryName");
        extractMetaData();
        try { urlForNaturalSearch = new URL((String) processedItems.get("ViewItemURLForNaturalSearch")); } catch (MalformedURLException ex) {}
        
        /*new ShippingCostSummaryDescriptor(amount, null, eBayName)
        
        setShippingCostSummaryDescriptor(null)*/
        /*setShippingCostSummaryDescriptor*/
        
        return this;
    }
    
    @Override
    protected EBayItemFactory populate() throws UnableToPopulateItemException{
        try {
            return doRequest().processResponse().build();
        } catch (IOException | ParserConfigurationException ex) {
            throw new UnableToPopulateItemException(ex);
        }
    }
    
    private void processItem(String item) {
        NodeList currentItem = xmlResponse.getElementsByTagName(item);
        
        for (int i = 0; i < currentItem.getLength(); i++) {
            Node currentNode = currentItem.item(i);

            if (isNodeEmpty(currentNode)) {
                continue;
            }

            NamedNodeMap attrMap = currentNode.getAttributes();
            for (int j = 0; j < attrMap.getLength(); j++) {
                processedItems.put(item + ":" + attrMap.item(j).getNodeName(), attrMap.item(j).getTextContent());
            }

            NodeList childNodes = currentNode.getChildNodes();
            /* Current node has children */
            if (childNodes.getLength() > 1) {
                processedItems.put(item, processItemChildren(childNodes));
                continue;
            }

            /* When the xml has more than one element with the same tag name */
            if (currentItem.getLength() > 1) {
                if (!(processedItems.get(item) instanceof ArrayList)) {
                    processedItems.put(item, new ArrayList());
                }

                ((ArrayList) processedItems.get(item)).add(currentNode.getTextContent());
                continue;
            }

            /* Only one */
            processedItems.put(item, currentNode.getTextContent());
        }
    }

    private ArrayList processItemChildren(NodeList childNodes) {
        ArrayList returnData = new ArrayList();

        for (int i = 0; i < childNodes.getLength(); i++) {
            Node subNode = childNodes.item(i);
            if (isNodeEmpty(subNode)) {
                continue;
            }

            if (subNode.getChildNodes().getLength() <= 1) {
                returnData.add(subNode.getTextContent());
            } else {
                returnData.add(processItemChildren(subNode.getChildNodes()));
            }
        }

        return returnData;
    }
    
    @Override
    protected EBayItemFactory extractMetaData() {
        ArrayList meta = (ArrayList) processedItems.get("ItemSpecifics");

        for (Iterator iterator = meta.iterator(); iterator.hasNext();) {
            ArrayList next = (ArrayList) iterator.next();
            getMetaDescriptor().put((String) next.get(0), (String) next.get(1));
        }

        return this;
    }

    private List<NameValuePair> getqParams() {
        if (qParams.isEmpty()) {
            qParams.add(new BasicNameValuePair("callname", "GetSingleItem"));
            qParams.add(new BasicNameValuePair("responseencoding", "XML"));
            qParams.add(new BasicNameValuePair("appid", eBayAppID));
            qParams.add(new BasicNameValuePair("siteid", "0"));
            qParams.add(new BasicNameValuePair("version", "515"));
            qParams.add(new BasicNameValuePair("ItemID", getItemCode()));
            qParams.add(new BasicNameValuePair("IncludeSelector", "Description,ItemSpecifics,ShippingCosts"));
        }

        return qParams;
    }

    private String getEncodedUrlParams() {
        return URLEncodedUtils.format(getqParams(), "UTF-8");
    }

    private Boolean isNodeEmpty(Node node) {
        return node instanceof Text && node.getNodeValue().trim().equals("");
    }

    public URL getUrlForNaturalSearch() {
        return urlForNaturalSearch;
    }

    public String getPrimaryCategoryName() {
        return primaryCategoryName;
    }

    @Override
    final protected ModelItem save() throws SQLException {
        ModelItem item = super.save(); //To change body of generated methods, choose Tools | Templates.
        
        ItemMeta.add(item.getId(), EBayItemMetaEnum.PRIMARY_CATEGORY_NAME, getPrimaryCategoryName());
        ItemMeta.add(item.getId(), EBayItemMetaEnum.URL_FOR_SEARCH, getPrimaryCategoryName());
        
        return item;
    }
}
