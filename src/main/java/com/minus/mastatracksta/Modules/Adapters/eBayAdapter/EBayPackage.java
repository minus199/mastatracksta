package com.minus.mastatracksta.Modules.Adapters.eBayAdapter;

import Classes.AftershipAPIException;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;
import Exceptions.trackingDriver.afterShip.MoreThanOneCourierDetectedException;
import java.net.URL;
import java.sql.SQLException;

/**
 *
 * @author minus
 */
public class EBayPackage extends com.minus.mastatracksta.ShippingPackage.Package{
    public EBayPackage(String trackingNumber) throws IllegalAccessException, InstantiationException, SQLException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, Exception {
        super(trackingNumber);
    }

    public EBayPackage(Integer pkgID) throws Exception {
        super(pkgID);
    }
}
