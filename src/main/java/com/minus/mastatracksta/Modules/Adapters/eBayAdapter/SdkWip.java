/*
 * Copyright 2015 Your Organisation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.minus.mastatracksta.Modules.Adapters.eBayAdapter;

import com.ebay.sdk.ApiAccount;
import com.ebay.sdk.ApiCall;
import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.ApiException;
import com.ebay.sdk.ApiLogging;
import com.ebay.sdk.SdkException;
import com.ebay.sdk.SdkSoapException;
import com.ebay.soap.eBLBaseComponents.AbstractResponseType;
import com.ebay.soap.eBLBaseComponents.GeteBayOfficialTimeRequestType;

/**
 *
 * @author asafb
 */
public class SdkWip {
    public void meta2() {
        // set devId, appId, certId in ApiAccount
        ApiAccount account = new ApiAccount();
        account.setDeveloper(EBayItemFactory.eBayDEVID);
        account.setApplication(EBayItemFactory.eBayAppID);
        account.setCertificate(EBayItemFactory.eBayCertID);

        // set ApiAccount and token in ApiCredential
        ApiCredential credential = new ApiCredential();
        credential.setApiAccount(account);
        credential.seteBayToken("AgAAAA**AQAAAA**aAAAAA**vtASVg**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AEk4WpDpKBogmdj6x9nY+seQ**KQEDAA**AAMAAA**R4w8gU7MgG6KKLsuWp2/VzUvNwrFKGLtQNYCXQsgZSM0/PmVlls6PTYmFlyAlXipRsUnY2FjjucER3GI7V4oYg2CGICFJ/fojKh7Pw+Dq47HoAte2p00O+qrUhLJtkNKHyguxzy7UhTa2tl65wF1asZBMBp0eeQ+zDXB/Q+NK7igg5/s4oFRojvK5gyukz0JlogXwPo30B8jlRk0wBwzljQ+jVqaToDeHwO6GX15xJNEPQyzDjnIEpKMRLv/ao4RagI/9FZTFq2RBvSDc5xRHxWuLRe8PntQmsFULbfvqLvFpqhuoTY+9onEDu6Kn0XRiom6in6cFYf1ueLDCGgTZBlANJxugy42Cu2pw2FDLR806BC7KGDaZztRsqEf8SHxXoxvZJAE3HfJhHeubOTVl0zsbgIOq6cFoCplyJNGxj7ciOrIuijEXb40c2f7T0/dfoXG5FYWoUw3/AasFiwcSGC4n8PmVu9TZY1bEdF88mcZlgWE9sr7TVuyQ2xAdHSjM7TZf7fWIUKcJz1WBMW4uo1WdfFhq4GPkPk5sLboymLNGvTLcnClI2c7FN8x0EN8dxW2YlY3/pV4Y2Dah5jhkN2aZS2YeKDfDQI1DnhWbmfNi0dQVvgMGrKo8PTnVVzO7qKZfHbMWUo+WIA7wEFTxD51XSsmqB5m+ZfLvRAoUeanzyEtfT5MbiJzOJgvfNkSCkknzz4D6o0UAtm3APQDgGGKjjKyjIHFNswMMgMAEC+C82qmUlpuEBHeS7j113jt");

        // add ApiCredential to ApiContext
        ApiContext context = new ApiContext();
        context.setApiCredential(credential);

        // set eBay server URL to call
        context.setApiServerUrl("https://api.sandbox.ebay.com/wsapi");  // sandbox

        // set timeout in milliseconds - 3 minutes
        context.setTimeout(180000);

        // set wsdl version number
        context.setWSDLVersion("423");

        // turn on logging
        ApiLogging logging = new ApiLogging();

        logging.setLogSOAPMessages(true);
        logging.setLogExceptions(true);
        context.setApiLogging(logging);
        // create ApiCall object - we'll use it to make the call
        ApiCall call = new ApiCall(context);

// create soap api request and response objects
        GeteBayOfficialTimeRequestType request = new GeteBayOfficialTimeRequestType();
        AbstractResponseType response;

// make the call and handle the response
        try {
            response = call.executeByApiName("GeteBayOfficialTime", request);
            System.out.println(response);
        } catch (ApiException ae) {
            System.out.println(ae);
        } catch (SdkSoapException sse) {
            System.out.println(sse);
        } catch (SdkException se) {
            System.out.println(se);
        }
        /*input = ConsoleUtil.readString("Enter your eBay Authentication Token: ");
         cred.seteBayToken(input);
     

         input = ConsoleUtil.readString("Enter eBay SOAP server URL (e.g., https://api.ebay.com/wsapi): ");
         apiContext.setApiServerUrl(input);

         // [2] Ask for itemID.
         System.out.println("===== [2] Call GetItemCall ====");
         String itemIDStr = ConsoleUtil.readString("Enter ID of the item that you want to get: ");

         GetItemCall gc = new GetItemCall(apiContext);
         */
    }
}
