package com.minus.mastatracksta.Modules.Adapters.google.gmail;

import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.gson.JsonObject;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelRawEmails;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mortbay.util.SingletonList;

/**
 * @author minus
 */
public class MessageOnSucessCallback extends JsonBatchCallback {
    private Message msg;
    private int pageNumber;
    
    @Override
    public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) throws IOException {
        System.out.println("Failure fetching msg from gmail: " + e);
    }

    @Override
    public void onSuccess(Object t, HttpHeaders responseHeaders) throws IOException {
        msg = (Message) t;
        
        List<MessagePart> payloadParts = (List<MessagePart>) msg.getPayload().get("parts");
        if (payloadParts != null) {
            for (MessagePart payloadPart : payloadParts) {
                processPayloadPart(payloadPart);
            }

            return;
        }

        MessagePart payloadPart = (MessagePart) msg.get("payload");
        if (payloadPart != null) {
            processPayloadPart(payloadPart);
            return;
        }
        
        Logger.getLogger("Could not find payload.");
    }

    private boolean processPayloadPart(MessagePart payload) {
        List<MessagePart> bodyList = payload.getParts();

        // Normalize singel part msgs to array 
        if (bodyList == null) {
            bodyList = SingletonList.newSingletonList(payload);
        }

        // if for some reason... maybe throw something right na
        if (bodyList == null) {
            return false;
        }

        for (MessagePart body : bodyList) {
            try {
                if (!handlePayloadBody(body.getBody())) {
                    Logger.getLogger("something failed");
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MessageOnSucessCallback.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(MessageOnSucessCallback.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return true;
    }

    private boolean handlePayloadBody(MessagePartBody body) throws FileNotFoundException, UnsupportedEncodingException {
        byte[] decodedData = body.decodeData();
        String decodedDatastring = new String(decodedData, "UTF-8");
        String payloadJSON = null;
        String replaceAll = null;
        String labels = null;
        
        ModelRawEmails modelRawEmails = null;
        
        try {
             /*labels = msg.getLabelIds().toString();
            replaceAll = labels.replaceAll("\\[(.*?)\\]", "$1");*/
            modelRawEmails = new ModelRawEmails(msg.getHistoryId(), msg.getInternalDate(), msg.getPayload().toPrettyString() ,msg.getSnippet(),decodedDatastring);
            DtoFactory.getInstance().getRawEmailsDao().create(modelRawEmails);
        } catch (IOException ex) {
            payloadJSON = "error";
            labels= "error";
        } catch (SQLException ex) {
            Logger.getLogger(MessageOnSucessCallback.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("processed " + msg.getId() + "[" + msg.getThreadId() + "]");
        // proccesed klkkl 
        //writeToFile(string1);
        //System.out.println(string1);
        return true;
    }

    private void writeToFile(String string) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter("/tmp/logger.txt")) {
            out.append(string);
        }
    }
}
