package com.minus.mastatracksta.Modules.Controllers;

import Utils.MsgGen;
import com.minus.mastatracksta.MainApp;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.Threads.AddPackageThread;
import com.minus.mastatracksta.Threads.ThreadCompleteListener;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author asafb
 */
public class AddPackageController extends VBox implements Initializable, ThreadCompleteListener {
    @FXML
    private TextField trackingNumberInput;

    @FXML
    private ChoiceBox originChoices;

    @FXML
    private AnchorPane childPlaceholder;
    @FXML
    private ScrollPane childPlaceholderWrapper;

    @FXML
    private Button saveNewPackage;

    @FXML
    private TextField itemCode;

    @FXML
    private TextField ItemURL;

    private OriginEnum origin;

    private ItemFromKnownOriginController knownOriginController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setVisible(true);
        originChoices.setVisible(false);

        childPlaceholder.maxHeightProperty().bind(heightProperty());

        originChoices.setItems(FXCollections.observableArrayList(OriginEnum.values()));
        originChoices.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            onOriginSelect(observable, oldValue, newValue);
        });

        trackingNumberInput.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) -> {
            if (trackingNumberInput.getText().length() > 5) {
                originChoices.setVisible(true);
            }
        });

        saveNewPackage.setOnAction((ActionEvent e) -> {
            startPkgThread();
        });
    }

    private void startPkgThread() {
        AddPackageThread addPackageThread = new AddPackageThread(getTrackingNumberInput().getText(), origin, knownOriginController.getItemsData());
        addPackageThread.addListener(this);
        addPackageThread.start();
    }

    private void refreshPkgList() throws SQLException {
        MainViewController.getInstance().createPkgList();
    }

    @Override
    public void notifyOfThreadComplete(Thread thread) {
        Platform.runLater(new Thread(() -> {
            MsgGen.alert("New package", trackingNumberInput.getText(), "New package " + trackingNumberInput.getText() + " created successfuly.", Alert.AlertType.INFORMATION);
            try {
                refreshPkgList();
            } catch (SQLException ex) {
                Logger.getLogger(AddPackageController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }));
    }

    private void loadKnownOrigin() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemFromKnownOrigin.fxml"));
        AnchorPane pane = (AnchorPane) loader.load();
        knownOriginController = loader.<ItemFromKnownOriginController>getController();
        
        childPlaceholder.heightProperty().add(heightProperty());
        childPlaceholderWrapper.setMaxHeight(600);
        
        childPlaceholder.getChildren().setAll(pane);
    }

    private void loadUnknownOrigin() throws IOException {
        AnchorPane pane = (AnchorPane) FXMLLoader.load(getClass().getResource("/fxml/ItemFromUnknownOrigin.fxml"));

        childPlaceholder.getChildren().setAll(pane);
    }

    private void onOriginSelect(ObservableValue observable, Object oldValue, Object newValue) {
        try {
            origin = OriginEnum.valueOf(newValue.toString());
            switch (origin) {
                case EBAY:
                    loadKnownOrigin();
                    break;
                case ALIEXPRESS:
                case BANGGOD:
                case ZAMPLEBOX:
                case UNKNOWN:
                case UNSPECIFIED:
                    loadUnknownOrigin();
                    break;
                default:
                    throw new AssertionError(origin.name());
            }
        } catch (IOException ex) {
            Logger.getLogger(AddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public TextField getTrackingNumberInput() {
        return trackingNumberInput;
    }

    public void setTrackingNumberInput(TextField trackingNumberInput) {
        this.trackingNumberInput = trackingNumberInput;
    }

    public ChoiceBox getOriginChoices() {
        return originChoices;
    }

    public void setOriginChoices(ChoiceBox originChoices) {
        this.originChoices = originChoices;
    }

    public AnchorPane getChildPlaceholder() {
        return childPlaceholder;
    }

    public void setChildPlaceholder(AnchorPane childPlaceholder) {
        this.childPlaceholder = childPlaceholder;
    }

    public Button getSaveNewPackage() {
        return saveNewPackage;
    }

    public void setSaveNewPackage(Button saveNewPackage) {
        this.saveNewPackage = saveNewPackage;
    }

    public TextField getItemCode() {
        return itemCode;
    }

    public void setItemCode(TextField itemCode) {
        this.itemCode = itemCode;
    }

    public TextField getItemURL() {
        return ItemURL;
    }

    public void setItemURL(TextField ItemURL) {
        this.ItemURL = ItemURL;
    }

    public OriginEnum getOrigin() {
        return origin;
    }
}
