/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minus.mastatracksta.Modules.Controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author minus
 */
public class ItemFromKnownOriginController extends VBox implements Initializable {
    @FXML
    private Button plusItem;

    @FXML
    private VBox items;
    
    @FXML
    private VBox addKnownOriginContainer;

    @FXML
    private AnchorPane itemsContainer;
    
    private final ArrayList<newItemInputPair> itemsData;

    public ItemFromKnownOriginController() {
        this.itemsData = new ArrayList<>();
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        items.maxHeightProperty().bind(heightProperty());
        
        generateNewItemPanel();
        plusItem.setOnAction((ActionEvent e) -> {
            VBox newItemPanel = generateNewItemPanel();
            getItems().getChildren().add(newItemPanel);
        });
    }

    private VBox generateNewItemPanel() {
        TextField itemCode = new TextField();
        TextField itemURL = new TextField();

        itemsData.add(new newItemInputPair(itemCode, itemURL));

        HBox itemCodeContainer = new HBox(new Label("Item Code"), itemCode);
        itemCodeContainer.setPrefSize(470.0, 16.0);
        itemCodeContainer.paddingProperty().setValue(new Insets(5.0, 5.0, 5.0, 5.0));

        HBox itemURLContainer = new HBox(new Label("Item URL"), itemURL);
        itemURLContainer.setPrefSize(470.0, 16.0);
        itemURLContainer.paddingProperty().setValue(new Insets(5.0, 5.0, 5.0, 5.0));

        VBox container = new VBox(itemCodeContainer, itemURLContainer);
        container.setPrefHeight(62.0);
        container.setPrefWidth(361.0);
        container.paddingProperty().setValue(new Insets(5.0, 5.0, 5.0, 5.0));

        return container;
    }

    public Button getPlusItem() {
        return plusItem;
    }

    public void setPlusItem(Button plusItem) {
        this.plusItem = plusItem;
    }

    public VBox getItems() {
        return items;
    }

    public void setItems(VBox items) {
        this.items = items;
    }

    public AnchorPane getItemsContainer() {
        return itemsContainer;
    }

    public void setItemsContainer(AnchorPane itemsContainer) {
        this.itemsContainer = itemsContainer;
    }
    
    public ArrayList<newItemInputPair> getItemsData() {
        return itemsData;
    }

    public VBox getAddKnownOriginContainer() {
        return addKnownOriginContainer;
    }

    public void setAddKnownOriginContainer(VBox addKnownOriginContainer) {
        this.addKnownOriginContainer = addKnownOriginContainer;
    }
    
    public static class newItemInputPair {
        final private TextField itemCode;
        final private TextField itemURL;

        public newItemInputPair(TextField itemCode, TextField itemURL) {
            this.itemCode = itemCode;
            this.itemURL = itemURL;
        }

        public TextField getItemCode() {
            return itemCode;
        }

        public TextField getItemURL() {
            return itemURL;
        }
    }
}
