/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minus.mastatracksta.Modules.Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
/**
 * FXML Controller class
 *
 * @author minus
 */
public class ItemFromUnknownOriginController implements Initializable {
    private TextField price;
    private ChoiceBox location;
    private TextField shippingCost;
    private TextField title;
    private DatePicker purchaseDate;
    private ChoiceBox shippingCurrency;
    private TextField description;
    private ChoiceBox priceCurrency;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public TextField getPrice() {
        return price;
    }

    public void setPrice(TextField price) {
        this.price = price;
    }

    public ChoiceBox getLocation() {
        return location;
    }

    public void setLocation(ChoiceBox location) {
        this.location = location;
    }

    public TextField getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(TextField shippingCost) {
        this.shippingCost = shippingCost;
    }

    public TextField getTitle() {
        return title;
    }

    public void setTitle(TextField title) {
        this.title = title;
    }

    public DatePicker getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(DatePicker purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public ChoiceBox getShippingCurrency() {
        return shippingCurrency;
    }

    public void setShippingCurrency(ChoiceBox shippingCurrency) {
        this.shippingCurrency = shippingCurrency;
    }

    public TextField getDescription() {
        return description;
    }

    public void setDescription(TextField description) {
        this.description = description;
    }

    public ChoiceBox getPriceCurrency() {
        return priceCurrency;
    }

    public void setPriceCurrency(ChoiceBox priceCurrency) {
        this.priceCurrency = priceCurrency;
    }
}
