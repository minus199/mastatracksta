/*
 * Copyright 2015 Your Organisation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.minus.mastatracksta.Modules.Controllers;

import Utils.MsgGen;
import Classes.AftershipAPIException;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;
import Exceptions.trackingDriver.afterShip.MoreThanOneCourierDetectedException;
import Utils.StringUtils;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelPackage;
import com.minus.mastatracksta.Modules.PictureGallery.ImageCreator;
import com.minus.mastatracksta.ShippingPackage.Item.MainContentViewResourceBundle;
import com.minus.mastatracksta.Threads.OnFinishNotifyingThread;
import com.minus.mastatracksta.Threads.PreparePkgForDisplayThread;
import com.minus.mastatracksta.Threads.ThreadCompleteListener;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author minus
 */
public class MainViewController implements Initializable, ThreadCompleteListener {

    private static MainViewController instance;
    private com.minus.mastatracksta.ShippingPackage.Package pkg;

    @FXML
    private ColumnConstraints mainContentColumn;
    
    @FXML
    private ToolBar pkgMenu;
    @FXML
    private ListView<String> pkgItemList;

    @FXML
    private AnchorPane addPkgViewContainer;
    @FXML
    private StackPane addPkgView;

    @FXML
    private AnchorPane mainContentContainer;
    @FXML
    private StackPane mainContent;
    final private HashMap<String, BorderPane> selectedContent = new HashMap<>();

    @FXML
    private Button openAddPkgView;
    @FXML
    private Button openPkgMetaView;
    @FXML
    private Button openTrackingView;

    final private HashMap<String, Object> buttonsOnClickActions = new HashMap<>();

    public static MainViewController getInstance() {
        return instance;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance = this;
        Platform.setImplicitExit(false);

        try {
            initMenu();
            createPkgList();
            initEventListeners();
        } catch (Exception ex) {
            Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
            MsgGen.alertException(ex);
        }
    }

    private void initMenu() throws IOException, Exception {
        openAddPkgView.setGraphic(ImageCreator.makeIcon("addItem").getImageView());
        openPkgMetaView.setGraphic(ImageCreator.makeIcon("viewPackageActive").getImageView());
        openTrackingView.setGraphic(ImageCreator.makeIcon("tracking").getImageView());

        buttonsOnClickActions.put("NewPkgView", openAddPkgView);
        buttonsOnClickActions.put("PackageMetaView", openPkgMetaView);
        buttonsOnClickActions.put("TrackingView", openTrackingView);
    }

    private void initEventListeners() throws IOException {
        /* Package list -- On change*/
        pkgItemList.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                OnFinishNotifyingThread pkgProcessingThread = new PreparePkgForDisplayThread(newValue);
                pkgProcessingThread.addListener(this);
                pkgProcessingThread.start();
            } catch (Exception ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                MsgGen.alertException(ex);
            }
        });

        openAddPkgView.setOnAction((ActionEvent evt) -> {
            slideAddPkgView();
        });

        pkgMenu.addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent mouseEvent) -> {
            try {
                EventTarget target = mouseEvent.getTarget();
                String id = ((Button) target).getId().substring(4);
                if ("AddPkgView".equals(id)) {
                    initAddPkgView();
                    return;
                }

                if (pkg == null) {
                    MsgGen.alertInfo("Loading", "Please wait while preparing", "Still fetching data....");
                    return;
                }

                initView(id, pkg);
            } catch (ClassCastException e) {
                System.out.println("not a button?");
            } catch (IOException ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                MsgGen.alertException(ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                MsgGen.alertException(ex);
            } catch (AftershipAPIException ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                MsgGen.alertException(ex);
            } catch (Exception ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                MsgGen.alertException(ex);
            }
        });
    }

    private void slideAddPkgView() {
        TranslateTransition openNav = new TranslateTransition(new Duration(150), addPkgView);
        TranslateTransition closeNav = new TranslateTransition(new Duration(350), addPkgView);
        if (addPkgView.getTranslateX() != 0) {
            try {
                initAddPkgView();
                openNav.setToX(0);
                openNav.play();
                addPkgViewContainer.toFront();
                pkgItemList.toBack();
            } catch (IOException ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            closeNav.setToX(-(addPkgViewContainer.getWidth()));
            closeNav.play();
            addPkgViewContainer.toBack();
            pkgItemList.toFront();
        }
    }

    public MainViewController createPkgList() throws SQLException {
        pkgItemList.toFront();
        ArrayList<String> trackingNumbers = new ArrayList();

        List<ModelPackage> packages = DtoFactory.getInstance().getPackagesDao().queryForAll();
        if (packages.size() < 1) {
            pkgItemList.setPlaceholder(new Label("Add some packages..."));
            return this;
        }

        packages.forEach(pkg -> trackingNumbers.add(pkg.getTrackingNumber()));
        pkgItemList.getItems().clear();
        pkgItemList.setItems(FXCollections.observableArrayList(trackingNumbers));

        return this;
    }

    private void initAddPkgView() throws IOException {
        AnchorPane addPkgLayout = FXMLLoader.load(getClass().getResource("/fxml/AddPkgView.fxml"));
        addPkgView.getChildren().clear();
        addPkgView.getChildren().add(addPkgLayout);

        addPkgView.setAlignment(Pos.BASELINE_LEFT);
        addPkgViewContainer.autosize();
    }

    private void initView(String viewFileName, com.minus.mastatracksta.ShippingPackage.Package pkg)
            throws IllegalAccessException, IOException, InstantiationException, AftershipAPIException, Exception {

        String relativePath = "/fxml/" + viewFileName + (!viewFileName.contains("View") ? "View" : "") + ".fxml";
        BorderPane currentView = FXMLLoader.load(getClass().getResource(relativePath), new MainContentViewResourceBundle(pkg));
        refreshMainContent(currentView);
        selectedContent.put(currentView.getId(), currentView);
    }

    @Override //After preparing package for view is finished
    public void notifyOfThreadComplete(Thread thread) {
        PreparePkgForDisplayThread preparePkgForDisplayThread = (PreparePkgForDisplayThread) thread;
        pkg = preparePkgForDisplayThread.getPkg();
        Platform.runLater(() -> {
            String viewFile;
            if (mainContent.getChildren() != null && mainContent.getChildren().size() > 0) {
                viewFile = StringUtils.toUpper(mainContent.getChildren().get(0).getId());
            } else {
                viewFile = "PkgMeta";
            }

            try {
                initView(viewFile, pkg);
            } catch (IOException ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (AftershipAPIException ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

    }

    private MainViewController refreshMainContent(Node node) {
        mainContent.getChildren().clear();
        mainContent.getChildren().addAll(node);

        Parent parent = mainContent.getParent();
        while (parent != null){
            Parent currentParent = parent.getParent();
            if (currentParent == null)
                break;
            
            parent = currentParent;
        }
        
        try{
            GridPane currentParent = (GridPane)parent;
            Double prefWidth = currentParent.getWidth() - pkgItemList.getWidth() - pkgMenu.getWidth() ;
            mainContentColumn.setPrefWidth(prefWidth);
        }catch (ClassCastException | NullPointerException ex){
            MsgGen.alertException(ex);
        }
        
        return this;
    }
}
