package com.minus.mastatracksta.Modules.Controllers;

import com.minus.mastatracksta.MainApp;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import com.minus.mastatracksta.ShippingPackage.Item.MainContentViewResourceBundle;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Border;

/**
 * FXML Controller class
 *
 * @author minus
 */
public class ShowPackageController implements Initializable {

    private ObservableList<MainApp.MetaInfoModel> metaInfoData;

    @FXML
    private TableView pkgDetails;
    @FXML
    private TableColumn<MainApp.MetaInfoModel, String> metaName;
    @FXML
    private TableColumn<MainApp.MetaInfoModel, String> metaValue;

    @FXML
    private ShowPackageController controller;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MainContentViewResourceBundle bundle = (MainContentViewResourceBundle) rb;

        try {
            initPackage(bundle.getPkg());
        } catch (Exception ex) {
            Logger.getLogger(ShowPackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initPackage(com.minus.mastatracksta.ShippingPackage.Package pkg) throws IllegalAccessException, InstantiationException, SQLException, Exception {
        for (Item packageItem : pkg.getPackageItems()) {
            ArrayList<MainApp.MetaInfoModel> toMetaInfoModels = packageItem.toMetaInfoModels();
            metaInfoData = FXCollections.observableArrayList(toMetaInfoModels);
            fillMetaTable();
        }
    }

    public ShowPackageController fillMetaTable() {
        pkgDetails.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        pkgDetails.setBorder(Border.EMPTY);
        metaName.setCellValueFactory(new PropertyValueFactory<>("metaName"));
        metaValue.setCellValueFactory(new PropertyValueFactory<>("metaValue"));

        pkgDetails.getItems().setAll(metaInfoData);

        pkgDetails.autosize();
        return this;
    }

    public TableColumn<MainApp.MetaInfoModel, String> getMetaName() {
        return metaName;
    }

    public void setMetaName(TableColumn<MainApp.MetaInfoModel, String> metaName) {
        this.metaName = metaName;
    }

    public TableColumn<MainApp.MetaInfoModel, String> getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(TableColumn<MainApp.MetaInfoModel, String> metaValue) {
        this.metaValue = metaValue;
    }

    public ShowPackageController getController() {
        return controller;
    }

    public void setController(ShowPackageController controller) {
        this.controller = controller;
    }
}
