package com.minus.mastatracksta.Modules.Controllers;

import Utils.MsgGen;
import Classes.Courier;
import Classes.Tracking;
import com.minus.mastatracksta.DB.Models.ModelCheckpoint;
import com.minus.mastatracksta.ShippingPackage.Item.MainContentViewResourceBundle;
import com.minus.mastatracksta.ShippingPackage.Package;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.util.Callback;

/**
 * @author minus
 */
public class TrackingController extends BorderPane implements Initializable {
    private Package pkg;

    /* Checkpoints */
    @FXML
    private TableView<CheckpointDataModel> checkpoints;

    @FXML
    private TableColumn checkpointMsg;

    @FXML
    private TableColumn createdAt;

    @FXML
    private TableColumn lastSeen;

    @FXML
    private TableColumn city;

    @FXML
    private TableColumn country;

    @FXML
    private TableColumn status;

    /* Tracking metainfo */
    @FXML
    private Label packgeID;

    @FXML
    private Label creationDate;

    @FXML
    private Label courierNicename;

    @FXML
    private Label destinationCountry;

    @FXML
    private Label originCountry;

    public void hideHeaders(TableView table) {
        Pane header = (Pane) table.lookup("TableHeaderRow");
        header.setVisible(false);
        table.setLayoutY(-header.getHeight());
        table.autosize();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pkg = ((MainContentViewResourceBundle)resources).getPkg();
        initTable();
        try {
            populateTrackingInfo(pkg.getPackageTrackingManager().getCourier());
            populateCheckpoints(pkg.getPackageTrackingManager().getModelCheckpoints());
        } catch (SQLException ex) {
            Logger.getLogger(TrackingController.class.getName()).log(Level.SEVERE, null, ex);
            MsgGen.alertException(ex);
        }
    }

    private Tracking populateTrackingInfo(Courier courier) {
        Tracking tracking = pkg.getPackageTrackingManager().getTracking();

        getPackgeID().setText(tracking.getTrackingNumber());
        getCourierNicename().setText(courier.getName());
        getOriginCountry().setText(tracking.getOriginCountryISO3() != null ? tracking.getOriginCountryISO3().getName() : "");
        getDestinationCountry().setText(tracking.getDestinationCountryISO3() != null ? tracking.getDestinationCountryISO3().getName() : "");

        if (tracking.getCreatedAt() != null) {
            getCreationDate().setText(new SimpleDateFormat("E yyyy-MM-dd ', around' ha").format(tracking.getCreatedAt()));
        }

        return tracking;
    }

    private void initTable() {
        checkpointMsg.setCellValueFactory(new PropertyValueFactory<>("checkpointMsg"));
        createdAt.setCellValueFactory(new PropertyValueFactory<>("createdAt"));
        lastSeen.setCellValueFactory(new PropertyValueFactory<>("lastSeen"));

        city.setCellValueFactory(new PropertyValueFactory<>("city"));
        country.setCellValueFactory(new PropertyValueFactory<>("country"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));

        checkpointMsg.setCellFactory(new Callback<TableColumn<String, String>, TableCell<String, String>>() {
            @Override
            public TableCell<String, String> call(TableColumn<String, String> param) {
                TableCell<String, String> cell = new TableCell<>();
                
                Text text = new Text();
                cell.setGraphic(text);
                //cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
                text.wrappingWidthProperty().bind(cell.widthProperty());
                text.textProperty().bind(cell.itemProperty());
                cell.resize(cell.getWidth(), cell.getHeight()*1.2);
                
                return cell;
            }
        });
    }

    private void populateCheckpoints(List<ModelCheckpoint> modelCheckpoints) throws SQLException {
        ArrayList<CheckpointDataModel> models = new ArrayList<>();

        modelCheckpoints.forEach((ModelCheckpoint checkpoint) -> {
            models.add(new CheckpointDataModel(
                    checkpoint.getMessage(),
                    checkpoint.getTime(),
                    checkpoint.getCreatedAt(),
                    checkpoint.getCity(),
                    checkpoint.getCountry() != null ? checkpoint.getCountry().getName() : "",
                    checkpoint.getTag()
            ));
        });

        final ObservableList<CheckpointDataModel> checkpointsData = FXCollections.observableArrayList(models);
        getCheckpoints().setBorder(Border.EMPTY);
        getCheckpoints().getItems().setAll(checkpointsData);
        getCheckpoints().setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);

//        ObjectProperty<Callback<TableColumn.CellDataFeatures<CheckpointDataModel, ?>, ObservableValue<?>>> cellValueFactoryProperty = (ObjectProperty<Callback<TableColumn.CellDataFeatures<CheckpointDataModel, ?>, ObservableValue<?>>>) checkpoints.getColumns().get(0).cellValueFactoryProperty();
    }

    public Label getPackgeID() {
        return packgeID;
    }

    public void setPackgeID(Label packgeID) {
        this.packgeID = packgeID;
    }

    public TableColumn getCheckpointMsg() {
        return checkpointMsg;
    }

    public void setCheckpointMsg(TableColumn checkpointMsg) {
        this.checkpointMsg = checkpointMsg;
    }

    public Label getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Label creationDate) {
        this.creationDate = creationDate;
    }

    public Label getCourierNicename() {
        return courierNicename;
    }

    public void setCourierNicename(Label courierNicename) {
        this.courierNicename = courierNicename;
    }

    public Label getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(Label destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public Label getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(Label originCountry) {
        this.originCountry = originCountry;
    }

    public TableColumn getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(TableColumn createdAt) {
        this.createdAt = createdAt;
    }

    public TableColumn getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(TableColumn lastSeen) {
        this.lastSeen = lastSeen;
    }

    public TableColumn getCity() {
        return city;
    }

    public void setCity(TableColumn city) {
        this.city = city;
    }

    public TableColumn getCountry() {
        return country;
    }

    public void setCountry(TableColumn country) {
        this.country = country;
    }

    public TableColumn getStatus() {
        return status;
    }

    public void setStatus(TableColumn status) {
        this.status = status;
    }

    public TableView getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(TableView checkpoints) {
        this.checkpoints = checkpoints;
    }

    public static class CheckpointDataModel {

        private String lastSeen;
        private String createdAt;
        private String city;
        private String country;
        private String status;
        private String checkpointMsg;

        public CheckpointDataModel(String message, String lastSeen, String createdAt, String city, String country, String status) {
            this.lastSeen = lastSeen;
            this.createdAt = createdAt;
            this.city = city;
            this.country = country;
            this.status = status;
            this.checkpointMsg = message;
        }

        public String getLastSeen() {
            return lastSeen;
        }

        public void setLastSeen(String lastSeen) {
            this.lastSeen = lastSeen;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String CreatedAt) {
            this.createdAt = CreatedAt;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCheckpointMsg() {
            return checkpointMsg;
        }

        public void setCheckpointMsg(String checkpointMsg) {
            this.checkpointMsg = checkpointMsg;
        }
    }
}
