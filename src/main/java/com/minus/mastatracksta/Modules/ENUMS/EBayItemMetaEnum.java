package com.minus.mastatracksta.Modules.ENUMS;

/**
 * @author minus
 */
public enum EBayItemMetaEnum implements BaseMetaEnum {
    DESCRIPTION, ITEM_URL, PRIMARY_CATEGORY_NAME, URL_FOR_SEARCH;
}
