/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minus.mastatracksta.Modules.Owner;

import java.util.ArrayList;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 *
 * @author minus
 */
public class Owner {

    private String firstName;
    private String lastName;
    private ArrayList<String> emails = new ArrayList<>();
    private ArrayList<String> phoneNumbers = new ArrayList<>();

    public Owner(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emails = emails;
        for (String email : emails) {
            if (!isValidEmailAddress(email)) {

            }
        }
        this.phoneNumbers = phoneNumbers;
    }

    public Owner addEmail(String email) {
        if (isValidEmailAddress(email)){
            emails.add(email);
            return this;
        }
        
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Invalid email");
        alert.setHeaderText("Looks like the email might be invalid.");
        alert.setContentText("Are you ok with this?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            // ... user chose OK
        } else {
            // ... user chose CANCEL or closed the dialog
        }
        
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ArrayList<String> getEmails() {
        return emails;
    }

    public ArrayList<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    final static public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

//    public void getAllPackages(){
//        DBManager.getINSTANCE().getDao(com.minus.mastatracksta.DB.Package.class)
//        
//        packageItemDao.forEach(pkg -> System.out.println(pkg.getId().toString()));
//    }
}
