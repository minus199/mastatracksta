package com.minus.mastatracksta.Modules.PictureGallery;

import Utils.Config;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.minus.mastatracksta.DB.DBManager;
import com.minus.mastatracksta.DB.DTO.ImageDTO;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelImage;
import com.minus.mastatracksta.DB.Models.ModelItemPicture;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import javax.imageio.ImageIO;

/**
 * @author minus
 */
public class ImageCreator {
    final private static String CONFIG_ICON_PREFIX = "icon.";
    
    private final URL pictureURL;

    private ModelItemPicture modelItemPicture;
    private ModelImage modelImage;

    private Dao daoItemPictures;
    private final Dao daoImages;

    private ImageCreator(URL url) throws SQLException, IOException {
        daoImages = DtoFactory.getInstance().getImageDao();
        pictureURL = url;
    }

    public static ItemPicture makeItemPicture(Item item, URL url) throws IOException, SQLException {
        ImageCreator instance = new ImageCreator(url);
        instance.daoItemPictures = DtoFactory.getInstance().getItemPicturesDao();

        return new ItemPicture(instance._persistImage().bindItem(item).modelItemPicture);
    }

    public static MenuIcon makeIcon(String icon) throws SQLException, IOException, Exception {
        String a = "/menu-icons/" + Config.getInstance().getConfigValue(CONFIG_ICON_PREFIX + icon);
        
        URL resource = ImageCreator.class.getResource(a);
        ImageCreator instance = new ImageCreator(resource);
        
        return new MenuIcon(instance._persistImage().modelImage);
    }

    private ImageCreator _persistImage() throws IOException, SQLException {
        BufferedImage img = ImageIO.read(pictureURL);
        byte[] bytesArray = getBytesArray();
        
        String generatedHash = generateHash(bytesArray);
        
        if ((modelImage = ImageDTO.getInstance().getByHash(generatedHash)) == null){
            modelImage = new ModelImage(generatedHash, bytesArray);
            daoImages.create(modelImage);
            daoImages.refresh(modelImage);
        }

        return this;
    }

    private ImageCreator bindItem(Item item) throws SQLException {
        modelItemPicture = new ModelItemPicture(item.getModel(), modelImage, pictureURL);
        daoItemPictures.create(modelItemPicture);

        return this;
    }

    private String generateHash(byte[] bytes) {
        return Integer.toString(Math.abs(Arrays.hashCode(bytes)));
    }

    private byte[] getBytesArray() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = pictureURL.openStream();

        byte[] byteChunk = new byte[4096];
        int n;

        while ((n = is.read(byteChunk)) > 0) {
            baos.write(byteChunk, 0, n);
        }

        if (is != null) {
            is.close();
        }

        return baos.toByteArray();
    }

    /*
     @Deprecated try to deprecate
     */
    private byte[] getByteArrayFromURL() throws IOException {
        byte[] result = null;
        FileInputStream fileInStr = null;
        try {
            BufferedImage img = ImageIO.read(pictureURL);

            File imgFile = new File("downloaded.jpg");
            imgFile.deleteOnExit();
            ImageIO.write(img, "jpg", imgFile);

            fileInStr = new FileInputStream(imgFile);
            long imageSize = imgFile.length();
            if (imageSize > Integer.MAX_VALUE) {
                return null;
            }

            if (imageSize > 0) {
                result = new byte[(int) imageSize];
                fileInStr.read(result);
            }

        } finally {
            if (fileInStr != null) {
                fileInStr.close();
            }
        }

        return result;
    }
}
