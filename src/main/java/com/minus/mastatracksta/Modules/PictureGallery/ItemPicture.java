package com.minus.mastatracksta.Modules.PictureGallery;

import com.minus.mastatracksta.DB.Models.ModelItemPicture;
import java.io.IOException;

/**
 *
 * @author minus
 */
public class ItemPicture extends Picture{
    public ItemPicture(ModelItemPicture modelItemPicture) throws IOException {
        super(modelItemPicture.getModelImage());
    }
}
