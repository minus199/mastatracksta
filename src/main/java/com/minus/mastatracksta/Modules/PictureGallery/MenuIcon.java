package com.minus.mastatracksta.Modules.PictureGallery;

import com.minus.mastatracksta.DB.Models.ModelImage;
import java.io.IOException;

/**
 * @author minus
 */
public class MenuIcon extends Picture{
    public MenuIcon(ModelImage modelImage) throws IOException {
        super(modelImage);
        getImageView().setFitWidth(27);
    }
}
