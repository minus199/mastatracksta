package com.minus.mastatracksta.Modules.PictureGallery;

import com.minus.mastatracksta.DB.Models.ModelImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author minus
 */
public class Picture {
    private final ModelImage modelImage;
    private javafx.scene.image.Image javaFxImage;
    private ImageView imageView;

    public Picture(ModelImage modelImage) throws IOException {
        this.modelImage = modelImage;
        setJavafxImg().setImageView();
    }
    
    private Picture setJavafxImg() throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(modelImage.getImageBytes());
        javaFxImage = new javafx.scene.image.Image(in);  
        return this;
    }
    
    private Picture setImageView(){
        imageView = new ImageView(javaFxImage);
        imageView.setPreserveRatio(true);
        return this;
    }

    public Image getJavaFxImage() {
        return javaFxImage;
    }

    public ImageView getImageView() {
        return imageView;
    }
}
