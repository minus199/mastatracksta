package com.minus.mastatracksta.Modules.PictureGallery;

import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author minus
 */
public class PictureGalleryFactory {

    private ArrayList<URL> urls;
    private final Item item;

    public PictureGalleryFactory(Item item, ArrayList<URL> urls) throws SQLException {
        this.urls = urls;
        this.item = item;
    }

    public PictureGalleryImpl create() throws IOException, Exception {
        ArrayList<ItemPicture> itemPictures = new ArrayList();

        for (URL url : urls) {
            itemPictures.add(ImageCreator.makeItemPicture(item, url));
        }

        return new PictureGalleryImpl(item, itemPictures);
    }

    public static PictureGalleryFactory factory(Item item, ArrayList<URL> urls) throws SQLException {
        return new PictureGalleryFactory(item, urls);
    }

    public Item getItem() {
        return item;
    }
}
