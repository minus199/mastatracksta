package com.minus.mastatracksta.Modules.PictureGallery;

import Exceptions.FoundZeroModelImagesException;
import com.minus.mastatracksta.DB.DTO.ItemPicturesDTO;
import com.minus.mastatracksta.DB.Models.ModelItemPicture;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author minus
 */
public class PictureGalleryImpl implements IGallery {
    final private Item item;
    final private ArrayList<ItemPicture> itemPictures;
    final private ArrayList<ImageView> imageViews = new ArrayList();

    /**
     *
     * @param item
     * @throws SQLException
     * @throws IOException
     * @throws Exceptions.FoundZeroModelImagesException
     */
    public PictureGalleryImpl(Item item) throws SQLException, IOException, FoundZeroModelImagesException, Exception {
        this.item = item;
        itemPictures = new ArrayList<>();
        //ArrayList imgList = item.getPictureGallery().getImages();
        loadItemPictures();
    }

    public PictureGalleryImpl(Item item, ArrayList<ItemPicture> itemPictures) {
        this.item = item;
        this.itemPictures = itemPictures;
    }
    
    private PictureGalleryImpl loadItemPictures() throws Exception{
        for (ModelItemPicture modelItemPicture : ItemPicturesDTO.getInstance().getByItemID(item.getId())) {
            ItemPicture itemPicture = new ItemPicture(modelItemPicture);
            itemPictures.add(itemPicture);
            ImageView imageView = itemPicture.getImageView();
            imageView.setFitWidth(600);
            imageView.setFitHeight(300);
            imageView.onMouseClickedProperty().addListener((ObservableValue<? extends EventHandler<? super MouseEvent>> observable, EventHandler<? super MouseEvent> oldValue, EventHandler<? super MouseEvent> newValue) -> {
                System.out.println("image clicked");
            });
            
            imageViews.add(imageView);
        }
        
        return this;
    }
    
    /**
     *
     * @return
     */
    @Override
    public Item getItem() {
        return item;
    }

    @Override
    public ArrayList<ItemPicture> getItemPictures() {
        return itemPictures;
    }

    public ArrayList<ImageView> getImageViews() {
        return imageViews;
    }
}
