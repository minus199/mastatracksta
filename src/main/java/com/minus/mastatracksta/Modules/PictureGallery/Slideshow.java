package com.minus.mastatracksta.Modules.PictureGallery;

import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.util.ArrayList;
import java.util.List;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class Slideshow {

    public static final double IMG_WIDTH = 600; //pixels
    public static final double IMG_HEIGHT = 300; //pixels

    private static final int NUM_OF_IMGS = 3;
    private static final int SLIDE_FREQ = 4; //secs

    private final Pane gallery = new Pane();
    final private Item item;
    private final HBox imgContainer  = new HBox();;

    public Slideshow(Item item) {
        this.item = item;
    }

    final public Slideshow generate(final Boolean startAnimation) throws Exception {
        build();

        if (startAnimation) {
            startAnimation();
        }

        return this;
    }

    private Slideshow build() throws Exception {
        // To center the slide show incase maximized
        gallery.setMaxSize(IMG_WIDTH, IMG_HEIGHT);
        gallery.setClip(new Rectangle(IMG_WIDTH, IMG_HEIGHT));

        imgContainer.getChildren().addAll(item.getPictureGallery().getImageViews());
        gallery.getChildren().add(imgContainer);

        return this;
    }

    final public void startAnimation() {
        EventHandler<ActionEvent> slideAction = (ActionEvent t) -> {
            TranslateTransition trans = new TranslateTransition(Duration.seconds(1.5), imgContainer);
            trans.setByX(-IMG_WIDTH);
            trans.setInterpolator(Interpolator.EASE_BOTH);
            trans.play();
        };

        EventHandler<ActionEvent> resetAction = (ActionEvent t) -> {
            TranslateTransition trans = new TranslateTransition(Duration.seconds(1), imgContainer);
            trans.setByX((NUM_OF_IMGS - 1) * IMG_WIDTH);
            trans.setInterpolator(Interpolator.EASE_BOTH);
            trans.play();
        };

        List<KeyFrame> keyFrames = new ArrayList<>();
        for (int i = 1; i <= NUM_OF_IMGS; i++) {
            if (i == NUM_OF_IMGS) {
                keyFrames.add(new KeyFrame(Duration.seconds(i * SLIDE_FREQ), resetAction));
                continue;
            }

            keyFrames.add(new KeyFrame(Duration.seconds(i * SLIDE_FREQ), slideAction));
        }

        Timeline anim = new Timeline(keyFrames.toArray(new KeyFrame[NUM_OF_IMGS]));
        gallery.setOnMouseClicked((MouseEvent event) -> {
            anim.pause();
        });
        
        anim.setCycleCount(Timeline.INDEFINITE);
        anim.playFromStart();
    }

    public Item getItem() {
        return item;
    }

    public Pane getGallery() {
        return gallery;
    }
}
