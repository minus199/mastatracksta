package com.minus.mastatracksta.Modules.Tracking;

import Classes.AftershipAPIException;
import Classes.Checkpoint;
import Classes.Courier;
import Classes.Tracking;
import Enums.ISO3Country;
import Exceptions.trackingDriver.UnableToConnectException;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;
import Exceptions.trackingDriver.afterShip.MoreThanOneCourierDetectedException;
import com.minus.mastatracksta.DB.DTO.CheckpointDTO;
import com.minus.mastatracksta.DB.DTO.TrackingDTO;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelCheckpoint;
import com.minus.mastatracksta.DB.Models.ModelTracking;
import com.minus.mastatracksta.Modules.Adapters.AfterShipAdapter.TrackingDriverBase;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author minus
 */
public class PackageTrackingManager {

    final private TrackingDriverBase trackingDriver;
    private ModelTracking modelTracking = new ModelTracking();
    private List<ModelCheckpoint> modelCheckpoints = new ArrayList<>();
    private Tracking preTracking;
    private Courier courier;

    private PackageTrackingManager(TrackingDriverBase trackingDriverBase, com.minus.mastatracksta.ShippingPackage.Package pkg) throws SQLException, AftershipAPIException, IOException, ParseException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        trackingDriver = trackingDriverBase;
        populateInitialTracking(pkg);
    }

    /**
     * * Since tracking is final: Initial Tracking object passed to this class
     * can contain only trackingNumber (which is injected via the constructor)
     * But any further details can be added to the passed Tracking object in
     * order for them to be persisted and consider.
     *
     * @param trackingDriverBase
     * @param pkg
     * @return
     * @throws java.sql.SQLException
     */
    final public static PackageTrackingManager factory(TrackingDriverBase trackingDriverBase, com.minus.mastatracksta.ShippingPackage.Package pkg) throws SQLException, ParseException, IOException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        PackageTrackingManager currentInstance = new PackageTrackingManager(trackingDriverBase, pkg);
        ModelTracking byTrackingNumber = TrackingDTO.getInstance().getByTrackingNumber(currentInstance.preTracking.getTrackingNumber());
        if (byTrackingNumber != null && byTrackingNumber.getUniqueToken() != null) { // Failsafe -- dont recreate trackings which are already exist
            return currentInstance.loadFromDB();
        }

        return currentInstance.populateInitialTracking(pkg).create();
    }

    final protected PackageTrackingManager create() throws SQLException, ParseException, IOException, BaseAftershipException, AftershipAPIException, UnableToConnectException {
        try {
            addTracking().refreshTracking().persistTracking().persistCheckpoints();
        } catch (AftershipAPIException ex) {
            try {
                throw new BaseAftershipException(ex); // throw in order to parse the json and get the error code
            } catch (BaseAftershipException aftershipException) {
                if (aftershipException.getErrorCode() == 4003.0) {
                    refreshTracking(); // found on afterShip but not in db
                } else {
                    throw aftershipException;
                }
            }
        } finally {
            assert preTracking.getUniqueToken() != null; // Good enough to know that models were populated correctly
        }

        return this;
    }

    /**
     *
     * For writing new data to tracking
     *
     * @return this
     * @throws IOException
     * @throws ParseException
     * @throws Exception
     */
    private PackageTrackingManager addTracking() throws IOException, ParseException, AftershipAPIException, UnableToConnectException {
        Tracking postTracking = trackingDriver.getConnection().postTracking(preTracking);
        preTracking = postTracking;

        return this;
    }

    final protected Tracking track() throws IOException, ParseException, SQLException, AftershipAPIException, UnableToConnectException {
        Tracking currentTracking = preTracking;
        Integer id = Integer.getInteger(currentTracking.getId());
        currentTracking.setId(null);
        preTracking = trackingDriver.getConnection().getTrackingByNumber(currentTracking);
        preTracking.setId(id != null ? id.toString() : null);
        
        return preTracking;
    }

    final public PackageTrackingManager refreshTracking() throws SQLException, IOException, ParseException, AftershipAPIException, UnableToConnectException {
        track();
        return persistTracking().persistCheckpoints();
    }

    /**
     
     * Puts the Tracking object passed as arguments to the tracker.
     * Updates tracking to the tracking fetched from afterShip.
     * after calling this method, one can invoke getTracking in order to see the results
     * 
     * @param tracking
     * @return
     * @throws AftershipAPIException
     * @throws IOException
     * @throws ParseException
     * @throws SQLException
     */
    final public PackageTrackingManager update(Tracking tracking) throws AftershipAPIException, IOException, ParseException, SQLException, UnableToConnectException {
        return updateTracking(tracking).persistTracking().persistCheckpoints();
    }

    private PackageTrackingManager updateTracking(Tracking tracking) throws AftershipAPIException, IOException, ParseException, UnableToConnectException {
        Tracking putTracking = trackingDriver.getConnection().putTracking(tracking);
        assert putTracking.equals(tracking);

        return this;
    }

    final protected Courier autoDetectCourier() throws AftershipAPIException, IOException, ParseException, MoreThanOneCourierDetectedException, UnableToConnectException {
        List<Courier> detectCouriers = trackingDriver.getConnection().detectCouriers(preTracking.getTrackingNumber());

        if (detectCouriers.size() > 1) {
            throw new MoreThanOneCourierDetectedException();
        }

        return courier = detectCouriers.get(0);
    }

    private PackageTrackingManager populateInitialTracking(com.minus.mastatracksta.ShippingPackage.Package pkg) throws SQLException, AftershipAPIException, IOException, ParseException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        preTracking = new Tracking(pkg.getTrackingNumber());
        
        preTracking.setSlug(autoDetectCourier().getSlug());
        if (pkg.getOwner() != null) {
            preTracking.setCustomerName(pkg.getOwner().getFirstName() + " " + pkg.getOwner().getLastName());
            pkg.getOwner().getEmails().forEach(email -> preTracking.addEmails(email));
            pkg.getOwner().getPhoneNumbers().forEach(number -> preTracking.addSmses(number));
        }

        pkg.getPackageItems().forEach((Item item) -> {
            preTracking.addCustomFields("item_" + item.getId().toString(), item.getModel().getTitle());
            preTracking.addCustomFields("item_" + item.getId().toString() + "_price", item.getModel().getPrice().toString());
        });

        return this;
    }

    private PackageTrackingManager loadFromDB() throws SQLException {
        modelTracking = TrackingDTO.getInstance().getByTrackingNumber(preTracking.getTrackingNumber());

        preTracking.setCustomerName(modelTracking.getCustomerName());
        preTracking.setDeliveryTime(modelTracking.getDeliveryDate());
        preTracking.setDestinationCountryISO3(modelTracking.getDestinationCountry());
        preTracking.setId(modelTracking.getId().toString());
        preTracking.setOrderID(modelTracking.getOrderID());
        preTracking.setOrderIDPath(modelTracking.getOrderIdPath());

        preTracking.setSlug(modelTracking.getSlug());
        preTracking.setTitle(modelTracking.getTitle());
        preTracking.setTrackingAccountNumber(modelTracking.getTrackingAccountNumber());
        preTracking.setTrackingPostalCode(modelTracking.getPostalCode());
        preTracking.setTrackingShipDate(modelTracking.getTrackingShipDate());
        preTracking.setUniqueToken(modelTracking.getUniqueToken());

        modelCheckpoints = CheckpointDTO.getInstance().getByTracking(modelTracking);

        return this;
    }

    private PackageTrackingManager persistTracking() throws SQLException {
        modelTracking.setSlug(preTracking.getSlug());
        modelTracking.setTrackingNumber(preTracking.getTrackingNumber());
        modelTracking.setTrackedCount(preTracking.getTrackedCount());
        modelTracking.setTag(preTracking.getTag());
        modelTracking.setOrderID(preTracking.getOrderID());
        modelTracking.setOrderIdPath(preTracking.getOrderIDPath());
        modelTracking.setCustomerName(preTracking.getCustomerName());
        modelTracking.setShipmentType(preTracking.getShipmentType());
        modelTracking.setSignedBy(preTracking.getSignedBy());
        modelTracking.setPostalCode(preTracking.getTrackingPostalCode());
        modelTracking.setOriginCountry(preTracking.getOriginCountryISO3());
        modelTracking.setDestinationCountry(preTracking.getDestinationCountryISO3());
        modelTracking.setShipmentPackageCount(preTracking.getShipmentPackageCount());
        modelTracking.setTitle(preTracking.getTitle());
        modelTracking.setSource(preTracking.getSource());
        modelTracking.setTrackingAccountNumber(preTracking.getTrackingAccountNumber());
        modelTracking.setUniqueToken(preTracking.getUniqueToken());
        modelTracking.setExpectedDelivery(preTracking.getExpectedDelivery());
        modelTracking.setTrackingShipDate(preTracking.getTrackingShipDate());
        modelTracking.setDeliveryDate(preTracking.getDeliveryTime());
        modelTracking.setCreatedAt(preTracking.getCreatedAt());
        modelTracking.setUpdatedAt(preTracking.getUpdatedAt());
        //modelTracking.setEmails(new Gson().toJson(tracking.getEmails()));

        int id;
        if (TrackingDTO.getInstance().getByTrackingNumber(preTracking.getTrackingNumber()) == null) {
            DtoFactory.getInstance().getTrackingDao().create(modelTracking);
        } else {
            DtoFactory.getInstance().getTrackingDao().update(modelTracking);
        }

        DtoFactory.getInstance().getTrackingDao().refresh(modelTracking);
        
        return this;
    }

    private PackageTrackingManager persistCheckpoints() throws SQLException {
        modelCheckpoints.clear();

        for (Checkpoint checkpoint : preTracking.getCheckpoints()) {
            ModelCheckpoint persistedCheckpoint = persistCheckpoint(checkpoint);
            modelCheckpoints.add(persistedCheckpoint);
        }

        return this;
    }

    private ModelCheckpoint persistCheckpoint(Checkpoint checkpoint) throws SQLException {
        ModelCheckpoint modelCheckpoint = new ModelCheckpoint();

        modelCheckpoint.setTime(checkpoint.getCheckpointTime());
        modelCheckpoint.setCity((String) (checkpoint.getCity() == null ? "" : checkpoint.getCity()));
        modelCheckpoint.setCountry(checkpoint.getCountryISO3() == null ? null : checkpoint.getCountryISO3());
        modelCheckpoint.setTracking(modelTracking);
        modelCheckpoint.setCreatedAt(checkpoint.getCreatedAt());
        modelCheckpoint.setMessage(checkpoint.getMessage());
        modelCheckpoint.setState(checkpoint.getState() == null ? "" : checkpoint.getState());
        modelCheckpoint.setTag(checkpoint.getTag());
        modelCheckpoint.setZipCode(checkpoint.getZip() == null ? "" : checkpoint.getZip());

        if (CheckpointDTO.getInstance().isExists(modelCheckpoint)) {
            DtoFactory.getInstance().getCheckpointDao().update(modelCheckpoint);
        } else {
            DtoFactory.getInstance().getCheckpointDao().create(modelCheckpoint);
        }

        return modelCheckpoint;
    }
    
    public List<ModelCheckpoint> getModelCheckpoints() throws SQLException {
        if (modelCheckpoints == null && modelTracking != null){
            modelCheckpoints = CheckpointDTO.getInstance().getByTracking(modelTracking);
        }
        
        return modelCheckpoints;
    }

    public Tracking getTracking() {
        return preTracking;
    }

    public Courier getCourier() {
        return courier;
    }
}
