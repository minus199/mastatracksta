package com.minus.mastatracksta.ShippingPackage.Descriptors;


/**
 *
 * @author minus
 */
public class ItemLocationDescriptor {
    private final String city;
    private final String country;

    public ItemLocationDescriptor(String city, String country) {
        this.city = city;
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        //return "Todo: fix " + getClass().getName() + " toString";
        return city.substring(0, 1).toUpperCase() + city.substring(1) + ", " +  country.substring(0, 1).toUpperCase() + country.substring(1);
    }
}