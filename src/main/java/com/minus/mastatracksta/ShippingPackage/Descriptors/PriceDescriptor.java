/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minus.mastatracksta.ShippingPackage.Descriptors;

import com.minus.mastatracksta.Modules.ENUMS.CurrencyEnum;

/**
 *
 * @author minus
 */
public class PriceDescriptor {
    private final CurrencyEnum currency;
    private final Float amount;

    public PriceDescriptor(CurrencyEnum currency, Float amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public CurrencyEnum getCurrency() {
        return currency;
    }

    public Float getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return amount + " " + currency;
    }
}
