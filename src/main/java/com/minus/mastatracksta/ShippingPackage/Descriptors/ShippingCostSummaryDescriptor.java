/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minus.mastatracksta.ShippingPackage.Descriptors;

import com.minus.mastatracksta.Modules.ENUMS.CurrencyEnum;

/**
 *
 * @author minus
 */
public class ShippingCostSummaryDescriptor {
    private final PriceDescriptor priceDescriptor;
    private final String shippingType;

    public ShippingCostSummaryDescriptor(float amount, CurrencyEnum currencyCode, String shippingType) {
        this.priceDescriptor = new PriceDescriptor(currencyCode, amount);
        this.shippingType = shippingType;
    }

    public PriceDescriptor getPriceDescriptor() {
        return priceDescriptor;
    }

    public String getShippingType() {
        return shippingType;
    }
}
