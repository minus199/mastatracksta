package com.minus.mastatracksta.ShippingPackage.Item;

import com.minus.mastatracksta.DB.DTO.ItemDTO;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.MainApp;
import com.minus.mastatracksta.Modules.ENUMS.CurrencyEnum;
import com.minus.mastatracksta.Modules.PictureGallery.PictureGalleryFactory;
import com.minus.mastatracksta.Modules.PictureGallery.PictureGalleryImpl;
import com.minus.mastatracksta.Modules.PictureGallery.Slideshow;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author minus
 */
public class Item {

    private Integer id;
    private PictureGalleryImpl gallery;
    final protected ModelItem modelItem;
    private ItemMeta itemMeta;

    public Item(String itemCode) throws SQLException, IOException, Exception {
        modelItem = ItemDTO.getInstance().getByItemCode(itemCode);
        id = modelItem.getId();
        setGallery();
    }

    final public PictureGalleryImpl getPictureGallery() throws SQLException, IOException {
        return gallery;
    }

    public ItemMeta getMeta() {
        if (itemMeta == null) {
            itemMeta = new ItemMeta(this);
        }

        return itemMeta;
    }

    public Item setGallery() throws SQLException, IOException, Exception {
        gallery = new PictureGalleryImpl(this);

        return this;
    }

    final public Item setGallery(ArrayList<URL> pictureURLs) throws SQLException, Exception {
        this.gallery = PictureGalleryFactory.factory(this, pictureURLs).create();
        return this;
    }

    public ArrayList<MainApp.MetaInfoModel> toMetaInfoModels() throws Exception {
        HashMap<String, Object> objectContent = new HashMap<>();

        objectContent.put("Id", getId().toString());
        objectContent.put("ItemCode", getItemCode());
        objectContent.put("title", getTitle());
        objectContent.put("currentPrice.amount", getPriceAsString());
        objectContent.put("currentPrice.currency", getPriceWithCurrency());
        objectContent.put("location", getLocation());
        objectContent.put("DateCreated", getDateCreatedAsString());

        /*objectContent.put("shippingCost", modelItem.getShippingCost().toString());
         objectContent.put("shippingCostCurrency", modelItem.getShippingCostCurrency().toString());*/
        ArrayList<MainApp.MetaInfoModel> metaData = new ArrayList<>();
        objectContent.entrySet().stream().forEach((entrySet) -> {
            metaData.add(new MainApp.MetaInfoModel(entrySet.getKey(), entrySet.getValue()));
        });

        WebView webView = new WebView();
        WebEngine engine = webView.getEngine();
        engine.loadContent(getDescription());
        metaData.add(new MainApp.MetaInfoModel("Description", webView));
        Pane galleryPane = new Slideshow(this).generate(Boolean.TRUE).getGallery();
        metaData.add(new MainApp.MetaInfoModel("Image gallery", galleryPane));

        return metaData;
    }

    final public Integer getId() {
        return id;
    }

    final public String getTitle() {
        return modelItem.getTitle();
    }

    final public Date getDateCreated() {
        return modelItem.getDateCreated();
    }

    final public String getDateCreatedAsString() {
        return modelItem.getDateCreated().toString();
    }

    final public String getDescription() {
        return modelItem.getDescription();
    }

    final public String getItemCode() {
        return modelItem.getItemCode();
    }

    final public String getLocation() {
        return modelItem.getLocation();
    }

    final public Float getPrice() {
        return modelItem.getPrice();
    }

    final public String getPriceAsString() {
        return modelItem.getPrice().toString();
    }

    final public Enum<CurrencyEnum> getCurrency() {
        return modelItem.getPriceCurrency();
    }

    final public String getPriceWithCurrency() {
        return getPrice().toString() + " " + getCurrency().name();
    }

    public ModelItem getModel() {
        return modelItem;
    }
}
