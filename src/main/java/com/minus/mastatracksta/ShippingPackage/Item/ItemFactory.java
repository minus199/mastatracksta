package com.minus.mastatracksta.ShippingPackage.Item;

import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.Modules.ENUMS.CurrencyEnum;
import Exceptions.UnableToPopulateItemException;
import com.j256.ormlite.dao.DaoManager;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.DB.DBManager;
import com.minus.mastatracksta.DB.DTO.ItemDTO;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.ShippingPackage.Descriptors.ItemLocationDescriptor;
import com.minus.mastatracksta.ShippingPackage.Descriptors.ItemMetaInfoDescriptor;
import com.minus.mastatracksta.ShippingPackage.Descriptors.PriceDescriptor;
import com.minus.mastatracksta.ShippingPackage.Descriptors.ShippingCostSummaryDescriptor;
import com.minus.mastatracksta.ShippingPackage.Item.resolvers.ByOriginItemResolver;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author minus
 */
abstract public class ItemFactory {

    private final ItemMetaInfoDescriptor metaDescriptor = new ItemMetaInfoDescriptor();
    final private ModelItem modelItem= new ModelItem();

    private final String itemCode;
    private String title;
    private String description;
    private URL itemURL;
    private OriginEnum origin;
    private com.minus.mastatracksta.ShippingPackage.Descriptors.ItemLocationDescriptor locationDescriptor;
    private final ArrayList<URL> pictureURLs = new ArrayList<>();

    private com.minus.mastatracksta.ShippingPackage.Descriptors.PriceDescriptor priceDescriptor;
    private ShippingCostSummaryDescriptor shippingCostSummaryDescriptor;

    public ItemFactory(String itemCode) {
        this.itemCode = itemCode;
    }

    public ItemFactory(String itemCode, OriginEnum originEnum) {
        this.itemCode = itemCode;
        this.origin = originEnum;
    }

    public ItemFactory(String itemCode, URL itemURL) {
        this.itemCode = itemCode;
        this.itemURL = itemURL;

        //this.originEnum = originEnum; 
        /* TODO: extract origin from url */
        /* TODO: try to find item by item code and origin */
    }

    final public Item create() throws SQLException, Exception {
        Item item = ByOriginItemResolver.resolve(populate().save(), origin);
        item.setGallery(pictureURLs);
        
        return item;
    }

    protected ModelItem save() throws SQLException {
        modelItem.setTitle(title);

        modelItem.setDescription(description);
        modelItem.setItemCode(itemCode);
        modelItem.setItemURL(itemURL);
        modelItem.setLocation(locationDescriptor.toString());
        modelItem.setDateCreated(new Date());

        if (priceDescriptor instanceof PriceDescriptor) {
            modelItem.setPrice(priceDescriptor.getAmount());
            modelItem.setPriceCurrency(priceDescriptor.getCurrency());
        }

        /*if (shippingCostSummaryDescriptor instanceof ShippingCostSummaryDescriptor){
         modelItem.setShippingCost(shippingCostSummaryDescriptor.getPriceDescriptor().getAmount());
         modelItem.setShippingCostCurrency(shippingCostSummaryDescriptor.getPriceDescriptor().getCurrency());
         }*/
        DtoFactory.getInstance().getItemDao().create(modelItem);
        DtoFactory.getInstance().getItemDao().refresh(modelItem);
        
        return modelItem;
    }

    abstract protected ItemFactory extractMetaData();

    abstract protected ItemFactory populate() throws UnableToPopulateItemException;

    public String getItemCode() {
        return itemCode;
    }

    public URL getItemURL() {
        return itemURL;
    }

    public String getTitle() {
        return title;
    }

    public ItemFactory setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ItemFactory setDescription(String description) {
        this.description = description;

        return this;
    }

    public PriceDescriptor getPriceDescriptor() {
        return priceDescriptor;
    }

    protected ItemFactory setPriceDescriptor(Float amount, CurrencyEnum currency) {
        this.priceDescriptor = new PriceDescriptor(currency, amount);
        return this;
    }

    protected ItemFactory setPriceDescriptor(String amount, String currency) {
        try {
            this.priceDescriptor = new PriceDescriptor(CurrencyEnum.valueOf(currency), Float.parseFloat(amount));
        } catch (IllegalArgumentException ex) {
            this.priceDescriptor = new PriceDescriptor(CurrencyEnum.UNRECOGNIZED, Float.parseFloat(amount));
        }

        return this;
    }

    public ShippingCostSummaryDescriptor getShippingCostSummaryDescriptor() {
        return shippingCostSummaryDescriptor;
    }

    protected ItemFactory setShippingCostSummaryDescriptor(ShippingCostSummaryDescriptor shippingCostSummaryDescriptor) {
        this.shippingCostSummaryDescriptor = shippingCostSummaryDescriptor;
        return this;
    }

    public OriginEnum getOriginEnum() {
        return origin;
    }

    public ItemLocationDescriptor getLocationDescriptor() {
        return locationDescriptor;
    }

    protected ItemFactory setLocationDescriptor(ItemLocationDescriptor locationDescriptor) {
        this.locationDescriptor = locationDescriptor;
        return this;
    }

    protected ItemFactory setLocationDescriptor(String city, String country) {
        this.locationDescriptor = new ItemLocationDescriptor(city, country);
        return this;
    }

    public ArrayList<URL> getPictureURLs() {
        return pictureURLs;
    }

    public ItemFactory setPictureURLs(ArrayList<String> pictureURLs) {
        for (int i = 0; i < pictureURLs.size(); i++) {
            try {
                this.pictureURLs.add(new URL(pictureURLs.get(i)));
            } catch (MalformedURLException ex) {
                /* Invalid url */
            }
        }

        return this;
    }

    public ItemFactory setPictureURLs(String pictureURL) {
        try {
            this.pictureURLs.add(new URL(pictureURL));
        } catch (MalformedURLException ex) {
            /* Invalid url */
        }
        return this;
    }

    public ItemMetaInfoDescriptor getMetaDescriptor() {
        return metaDescriptor;
    }

    public ModelItem getModelItem() {
        return modelItem;
    }

    public OriginEnum getOrigin() {
        return origin;
    }
}
