package com.minus.mastatracksta.ShippingPackage.Item;

import com.j256.ormlite.dao.Dao;
import com.minus.mastatracksta.DB.DTO.ItemMetaDTO;
import com.minus.mastatracksta.DB.Models.ModelItemMeta;
import com.minus.mastatracksta.Modules.ENUMS.EBayItemMetaEnum;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * @author minus
 */
public class ItemMeta {
    final private Item item;

    public ItemMeta(Item item) {
        this.item = item;
    }
    
    public HashMap<String, String> getAll() throws SQLException{
        HashMap output = new HashMap();
        
        List<ModelItemMeta> itemMetas = ItemMetaDTO.getInstance().getByItemId(item.getId());
        for (ModelItemMeta itemMeta : itemMetas) {
            output.put(itemMeta.getMetaKey(), itemMeta.getMetaValue());
        }
        
        return output;
    }
    
    public String getOne(EBayItemMetaEnum metaKey) throws SQLException{
        ModelItemMeta byItemIdAndMetaKey = ItemMetaDTO.getInstance().getByItemIdAndMetaKey(item.getId(), metaKey);
        if (byItemIdAndMetaKey != null)
            return byItemIdAndMetaKey.getMetaValue();
        
        return null;
    }
    
    public boolean create(EBayItemMetaEnum metaKey, String metaValue) throws SQLException{
        return createPut(item.getId(), metaKey, metaValue).isCreated();
    }
    
    public boolean update(EBayItemMetaEnum metaKey, String metaValue) throws SQLException{
        return createPut(item.getId(), metaKey, metaValue).isUpdated();
    }
    
    private static Dao.CreateOrUpdateStatus createPut(Integer itemID, EBayItemMetaEnum metaKey, String metaValue) throws SQLException{
        ModelItemMeta byItemIdAndMetaKey = ItemMetaDTO.getInstance().getByItemIdAndMetaKey(itemID, metaKey);
        if(byItemIdAndMetaKey == null){
            byItemIdAndMetaKey = new ModelItemMeta(itemID, metaKey, metaValue);
        } else {
            byItemIdAndMetaKey.setMetaValue(metaValue);
        }
        
        return ItemMetaDTO.getInstance().save(byItemIdAndMetaKey);
    }
    
    public static boolean add(Integer itemID, EBayItemMetaEnum metaKey, String metaValue) throws SQLException{
        return createPut(itemID, metaKey, metaValue).isCreated();
    }
}
