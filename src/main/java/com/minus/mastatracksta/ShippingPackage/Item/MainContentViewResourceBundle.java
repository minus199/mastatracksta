package com.minus.mastatracksta.ShippingPackage.Item;

import com.minus.mastatracksta.ShippingPackage.Package;
import java.util.ListResourceBundle;

/**
 *
 * @author minus
 */
public class MainContentViewResourceBundle extends ListResourceBundle {
    final private com.minus.mastatracksta.ShippingPackage.Package pkg;
    private String oldValue;
    private String newValue;

    public MainContentViewResourceBundle(com.minus.mastatracksta.ShippingPackage.Package pkg, String oldValue, String newValue) {
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.pkg = pkg;
    }
    
    public MainContentViewResourceBundle(com.minus.mastatracksta.ShippingPackage.Package pkg){
        this(pkg, null, null);
    }
    
    @Override
    protected Object[][] getContents() {
        return new Object[][] {
             {"OldValue", getOldValue()},
             {"NewValue", getNewValue()}
        };
    }

    public String getOldValue() {
        return oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public Package getPkg() {
        return pkg;
    }
    
    @Override
    public String toString() {
        return "NewItemResourceBundle{" + "oldValue=" + oldValue + ", newValue=" + newValue + '}';
    }
}
