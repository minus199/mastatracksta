package com.minus.mastatracksta.ShippingPackage.Item.resolvers;

import com.minus.mastatracksta.DB.DTO.ItemDTO;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.EBayItem;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.Interfaces.IResolver;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.sql.SQLException;

/**
 * @author minus
 */
public class ByIdItemResolver implements IResolver{
    private Integer itemID;
    
    private ByIdItemResolver(Integer itemID) {
        this.itemID = itemID;
    }
    
    public static Item resolve(Integer itemID) throws SQLException, Exception {
        ByIdItemResolver resolver = new ByIdItemResolver(itemID);

        return resolver._resolve();
    }

    private Item _resolve() throws SQLException, Exception {
        OriginEnum origin = ItemDTO.getInstance().getOrigin(itemID);
        
        switch (origin) {
            case EBAY:
                return new EBayItem(ItemDTO.getInstance().getByID(itemID).getItemCode());
            case ALIEXPRESS:
            case BANGGOD:
            case ZAMPLEBOX:
            case UNKNOWN:
            case UNSPECIFIED:
                throw new AssertionError(origin.name());
            default:
                throw new AssertionError(origin.name());
        }
    }
}
