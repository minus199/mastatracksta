package com.minus.mastatracksta.ShippingPackage.Item.resolvers;

import com.minus.mastatracksta.DB.DTO.ItemDTO;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.EBayItem;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.Interfaces.IResolver;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import java.sql.SQLException;

/**
 * @author minus
 */
public class ByModelItemResolver implements IResolver {
    private final ModelItem modelItem;
    
    private ByModelItemResolver(ModelItem modelItem) {
        this.modelItem = modelItem;
    }

    public static Item resolve(ModelItem modelItem) throws SQLException, Exception {
        ByModelItemResolver resolver = new ByModelItemResolver(modelItem);

        return resolver._resolve();
    }

    private Item _resolve() throws SQLException, Exception {
        OriginEnum origin = ItemDTO.getInstance().getOrigin(modelItem);
        switch (origin) {
            case EBAY:
                return new EBayItem(modelItem.getItemCode());
            case ALIEXPRESS:
            case BANGGOD:
            case ZAMPLEBOX:
            case UNKNOWN:
            case UNSPECIFIED:
                throw new AssertionError(origin.name());
            default:
                throw new AssertionError(origin.name());
        }
    }
}
