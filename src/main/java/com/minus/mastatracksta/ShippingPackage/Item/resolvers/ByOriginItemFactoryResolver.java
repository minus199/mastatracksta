package com.minus.mastatracksta.ShippingPackage.Item.resolvers;

import com.minus.mastatracksta.Modules.Adapters.AliExpressAdapter.AliExpressItemFactory;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import com.minus.mastatracksta.ShippingPackage.Item.ItemFactory;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.EBayItemFactory;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.Interfaces.IResolver;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * @author minus
 * Resolve ItemFactory by origin
 * itemCode is used to instantiate the correct ItemFactory
 */
public class ByOriginItemFactoryResolver implements IResolver{
    final private OriginEnum origin;
    final private String itemCode;

    public ByOriginItemFactoryResolver(String itemCode, OriginEnum origin) {
        this.origin = origin;
        this.itemCode = itemCode;
    }

    public static ItemFactory resolve(String itemCode, OriginEnum origin) throws SQLException{
        ByOriginItemFactoryResolver resolver = new ByOriginItemFactoryResolver(itemCode, origin);
        
        return resolver._resolve();
    }
    
    public static Item resolveAndCreate(String itemCode, OriginEnum origin) throws SQLException, Exception{
        ByOriginItemFactoryResolver resolver = new ByOriginItemFactoryResolver(itemCode, origin);
        
        return resolver._resolve().create();
    }

    private ItemFactory _resolve() throws SQLException {
        ItemFactory factory;

        switch (origin) {
            case EBAY:
                factory = new EBayItemFactory(itemCode, origin);
                break;
            case ALIEXPRESS:
            case BANGGOD:
            case ZAMPLEBOX:
            case UNKNOWN:
            case UNSPECIFIED:
                factory = new AliExpressItemFactory(itemCode, origin);
            default:
                throw new AssertionError("unable to find item.");
        }
        
        return factory;
    }
}
