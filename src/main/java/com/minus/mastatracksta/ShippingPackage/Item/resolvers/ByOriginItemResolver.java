package com.minus.mastatracksta.ShippingPackage.Item.resolvers;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.EBayItem;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.Interfaces.IResolver;
import java.sql.SQLException;

/**
 * @author minus Resolves ModelItem into Item object by origin
 */
public class ByOriginItemResolver implements IResolver {
    final private ModelItem modelItem;
    final private OriginEnum origin;

    public ByOriginItemResolver(ModelItem modelItem, OriginEnum origin) throws SQLException, IllegalAccessException, InstantiationException {
        this.modelItem = modelItem;
        this.origin = origin;
    }
    
    private ByOriginItemResolver(String itemCode, OriginEnum origin) throws SQLException, IllegalAccessException, InstantiationException {
        Dao dao = DtoFactory.getInstance().getItemDao();
        QueryBuilder builder = dao.queryBuilder();
        builder.where().eq("itemCode", itemCode);
        builder.limit(1);

        modelItem = (ModelItem) dao.query(builder.prepare()).get(0);
        this.origin = origin;
    }
    
    public static Item resolve(String itemCode, OriginEnum origin) throws SQLException, Exception {
        ByOriginItemResolver resolver = new ByOriginItemResolver(itemCode, origin);

        return resolver._resolve();
    }

    public static Item resolve(ModelItem modelItem, OriginEnum origin) throws SQLException, Exception {
        ByOriginItemResolver resolver = new ByOriginItemResolver(modelItem.getItemCode(), origin);

        return resolver._resolve();
    }

    private Item _resolve() throws SQLException, Exception {
        switch (origin) {
            case EBAY:
                return new EBayItem(modelItem.getItemCode());
            case ALIEXPRESS:
            case BANGGOD:
            case ZAMPLEBOX:
            case UNKNOWN:
            case UNSPECIFIED:
                throw new AssertionError(origin.name());
            default:
                throw new AssertionError(origin.name());
        }
    }
}
