package com.minus.mastatracksta.ShippingPackage.Item.resolvers;

import com.minus.mastatracksta.Modules.Adapters.AliExpressAdapter.AliExpressItemFactory;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import com.minus.mastatracksta.ShippingPackage.Item.ItemFactory;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.EBayItemFactory;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.Interfaces.IResolver;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * @author minus
 */
public class ByURLItemFactoryResolver implements IResolver{
    private OriginEnum origin;
    final private String itemCode;
    final private URL itemURL;

    public ByURLItemFactoryResolver(String itemCode, URL itemURL) throws MalformedURLException {
        this.itemCode = itemCode;
        this.itemURL = itemURL;
        this.origin = _resolveEnumByURL();
    }
    
    public static Item resolveAndCreate(String itemCode, URL itemURL) throws SQLException, Exception{
        ByURLItemFactoryResolver resolver = new ByURLItemFactoryResolver(itemCode, itemURL);
        
        return resolver._resolve().create();
    }
    
    private OriginEnum _resolveEnumByURL() throws MalformedURLException{
        String[] hostParts = itemURL.getHost().split(Pattern.quote("."));
        
        String originStr = "www".equals(hostParts[0]) ? hostParts[1] : hostParts[0];
        
        return OriginEnum.valueOf(!originStr.isEmpty() ? originStr.toUpperCase() : "UNSPECIFIED");
    }
    
    private ItemFactory _resolve() throws SQLException {
        ItemFactory factory;

        switch (origin) {
            case EBAY:
                factory = new EBayItemFactory(itemCode, origin);
                break;
            case ALIEXPRESS:
            case BANGGOD:
            case ZAMPLEBOX:
            case UNKNOWN:
            case UNSPECIFIED:
                factory = new AliExpressItemFactory(itemCode, origin);
            default:
                throw new AssertionError("unable to find item.");
        }
        
        return factory;
    }
}
