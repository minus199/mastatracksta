package com.minus.mastatracksta.ShippingPackage;

import Classes.AftershipAPIException;
import Classes.Tracking;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;
import Exceptions.trackingDriver.afterShip.MoreThanOneCourierDetectedException;
import com.minus.mastatracksta.DB.DTO.ItemDTO;
import com.minus.mastatracksta.DB.DTO.PackageDTO;
import com.minus.mastatracksta.DB.Models.ModelItem;
import com.minus.mastatracksta.DB.Models.ModelPackage;
import com.minus.mastatracksta.DB.Models.ModelPackageItems;
import com.minus.mastatracksta.Modules.Adapters.AfterShipAdapter.AftershipDriver;
import com.minus.mastatracksta.Modules.Controllers.TrackingController;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.Modules.Owner.Owner;
import com.minus.mastatracksta.Modules.Tracking.PackageTrackingManager;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import com.minus.mastatracksta.ShippingPackage.Item.resolvers.ByOriginItemResolver;
import com.minus.mastatracksta.Threads.FetchTrackingThread;
import com.minus.mastatracksta.Threads.ThreadCompleteListener;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author minus
 */
public class Package implements ThreadCompleteListener {

    private boolean isTrackingInitiated;
    public final static Class DAO_PACKAGE = com.minus.mastatracksta.DB.Models.ModelPackage.class;

    private final String trackingNumber;
    final private OriginEnum origin;

    final private ArrayList<Item> packageItems = new ArrayList<>();

    private ModelPackage modelPackage;
    private FetchTrackingThread fetchTrackingThread;
    private PackageTrackingManager packageTrackingManager;

    public Package(String trackingNumber) throws SQLException, IOException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        this.trackingNumber = trackingNumber;
        modelPackage = PackageDTO.getInstance().getByTrackingNumber(trackingNumber);
        origin = modelPackage.getOrigin();
    }

    public Package(Integer pkgID) 
            throws SQLException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        modelPackage = PackageDTO.getInstance().getByID(pkgID);
        this.trackingNumber = modelPackage.getTrackingNumber();
        origin = modelPackage.getOrigin();
    }

    final public Package process() 
            throws ParseException, IOException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        
        return process(null);
    }
    
    final public Package process(List<ThreadCompleteListener> trackingListeners) 
            throws ParseException, IOException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        return initTracking(trackingListeners);
    }
    
    private Package initTracking(List<ThreadCompleteListener> trackingListeners) 
            throws ParseException, IOException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        if (isTrackingInitiated) {
            return this;
        }

        packageTrackingManager = PackageTrackingManager.factory(new AftershipDriver(), this);
        fetchTrackingThread = new FetchTrackingThread(packageTrackingManager);
        // Will always add myself as a listener. Regards, Package.
        fetchTrackingThread.addListener(this);

        // Optional - Add additional listeners.
        if (trackingListeners != null && trackingListeners.size() > 0) {
            trackingListeners.forEach(listener -> fetchTrackingThread.addListener(listener));
        }

        fetchTrackingThread.start();

        return this;
    }

    public void toggleActive() {
        modelPackage.setIsActive(!modelPackage.getIsActive());
    }

    public void toggleArchived() {
        modelPackage.setIsActive(!modelPackage.getIsActive());
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public OriginEnum getOrigin() {
        return origin;
    }

    public Owner getOwner() {
        return new Owner("MiNuS", "Been Here");
    }

    public List<Item> getPackageItems() throws SQLException, Exception {
        if (packageItems != null && packageItems.size() > 0) {
            return packageItems;
        }

        return refreshPackageItems().packageItems;
    }

    protected Package refreshPackageItems() throws SQLException, Exception {
        packageItems.clear();

        List<ModelPackageItems> modelPackageItems = PackageDTO.getInstance().getModelPackageItems(modelPackage.getId());
        for (ModelPackageItems modelPackageItem : modelPackageItems) {
            ModelItem modelItem = ItemDTO.getInstance().getByID(modelPackageItem.getItem().getId());
            packageItems.add(ByOriginItemResolver.resolve(modelItem, origin));
        }

        return this;
    }

    private ModelPackage getModelPackage() throws SQLException, IllegalAccessException, InstantiationException {
        if (modelPackage instanceof ModelPackage) {
            return modelPackage;
        }

        return modelPackage = PackageDTO.getInstance().getByTrackingNumber(trackingNumber);
    }

    public PackageTrackingManager getPackageTrackingManager() {
        return packageTrackingManager;
    }

    public Integer getID() {
        return modelPackage.getId();
    }

    @Override
    public void notifyOfThreadComplete(Thread thread) {
        isTrackingInitiated = true;
    }
}
