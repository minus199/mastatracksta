package com.minus.mastatracksta.ShippingPackage;

import Classes.AftershipAPIException;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;
import Exceptions.trackingDriver.afterShip.MoreThanOneCourierDetectedException;
import com.j256.ormlite.dao.Dao;
import com.minus.mastatracksta.DB.DtoFactory;
import com.minus.mastatracksta.DB.Models.ModelPackage;
import com.minus.mastatracksta.DB.Models.ModelPackageItems;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import com.minus.mastatracksta.ShippingPackage.resolvers.ByOriginPackageResolver;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author minus
 */
public class PackageFactory {

    private ModelPackage modelPackage;
    final private static ArrayList<PackageFactory> loadedFactories = new ArrayList<>();
    final private static HashMap<Integer, Package> loadedPkgsByID = new HashMap<>();
    final private static HashMap<String, Package> loadedPkgsByTrackingNumber = new HashMap<>();

    private final ArrayList<ModelPackageItems> packageItems = new ArrayList();

    public static Get getBy() {
        return new Get();
    }

    public static class Get {
        public Package id(Integer pkgID) throws SQLException, IllegalAccessException, InstantiationException, Exception {
            Package pkg;
            if ((pkg = loadedPkgsByID.getOrDefault(pkgID, null)) != null) {
                return pkg;
            }

            Dao packageDao = DtoFactory.getInstance().getPackagesDao();
            ModelPackage modelPackage = (ModelPackage) packageDao.queryForId(pkgID);

            pkg = ByOriginPackageResolver.resolve(modelPackage);
            cache(null, pkg);

            return pkg;
        }

        public Package trackingNumber(String trackingNumber) throws SQLException, IllegalAccessException, InstantiationException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, Exception {
            Package pkg = new Package(trackingNumber);

            return pkg;
        }

        public Package origin(String trackingNumber, OriginEnum originEnum) throws IllegalAccessException, InstantiationException, Exception {
            return ByOriginPackageResolver.resolve(trackingNumber, originEnum);
        }
    }

    public static Package create(String trackingNumber, OriginEnum origin, ArrayList<Item> items)
            throws IOException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        PackageFactory instance = new PackageFactory();
        return instance.new Creator().create(trackingNumber, origin, items);
    }

    private class Creator {
        public Package create(String trackingNumber, OriginEnum origin, ArrayList<Item> items)
                throws SQLException, IOException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
            setModelPackage(trackingNumber, origin);
            DtoFactory.getInstance().getPackagesDao().create(modelPackage);

            if (items != null) items.forEach(item -> {
                try {
                    packageItems.add(createModelPackageItem(item));
                } catch (SQLException ex) {
                    System.out.println("Unable to bind item to package");
                }
            });

            Package pkg = new Package(modelPackage.getId());
            cache(PackageFactory.this, pkg);
            return pkg;
        }

        private Creator setModelPackage(String trackingNumber, OriginEnum origin) {
            modelPackage = new ModelPackage(null);

            modelPackage.setTrackingNumber(trackingNumber);
            modelPackage.setOrigin(origin);
            modelPackage.setIsActive(true);

            return this;
        }

        private ModelPackageItems createModelPackageItem(Item item) throws SQLException {
            ModelPackageItems packageItem = new ModelPackageItems(modelPackage, item.getModel());
            DtoFactory.getInstance().getPackageItemsDao().create(packageItem);

            return packageItem;
        }
    }

    private static void cache(PackageFactory instance, Package pkg) {
        if (instance != null && !loadedFactories.contains(instance)) {
            loadedFactories.add(instance);
        }

        if (!loadedPkgsByID.containsKey(pkg.getID())) {
            loadedPkgsByID.put(pkg.getID(), pkg);
        }

        if (!loadedPkgsByTrackingNumber.containsKey(pkg.getTrackingNumber())) {
            loadedPkgsByTrackingNumber.put(pkg.getTrackingNumber(), pkg);
        }
    }

    public ArrayList<ModelPackageItems> getPackageItems() {
        return packageItems;
    }
}
