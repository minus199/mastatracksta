package com.minus.mastatracksta.ShippingPackage.resolvers;

import Classes.AftershipAPIException;
import com.minus.mastatracksta.DB.Models.ModelPackage;
import com.minus.mastatracksta.Modules.Adapters.AliExpressAdapter.AliExpressPackage;
import com.minus.mastatracksta.Modules.Adapters.eBayAdapter.EBayPackage;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import java.sql.SQLException;

/**
 * @author minus
 */
public class ByOriginPackageResolver {

    final private OriginEnum origin;
    final private String trackingNumber;

    private ByOriginPackageResolver(String trackingNumber, OriginEnum origin) {
        this.origin = origin;
        this.trackingNumber = trackingNumber;
    }
    
    private ByOriginPackageResolver(ModelPackage modelPackage) {
        this.origin = (OriginEnum)modelPackage.getOrigin();
        this.trackingNumber = modelPackage.getTrackingNumber();
    }

    public static com.minus.mastatracksta.ShippingPackage.Package resolve(String trackingNumber, OriginEnum origin) throws SQLException, IllegalAccessException, InstantiationException, Exception {
        ByOriginPackageResolver resolver = new ByOriginPackageResolver(trackingNumber, origin);

        return resolver._resolve();
    }
    
    public static com.minus.mastatracksta.ShippingPackage.Package resolve(ModelPackage modelPackage) throws SQLException, IllegalAccessException, InstantiationException, Exception{
        ByOriginPackageResolver resolver = new ByOriginPackageResolver(modelPackage);

        return resolver._resolve();
    } 

    private com.minus.mastatracksta.ShippingPackage.Package _resolve() throws SQLException, IllegalAccessException, InstantiationException, AftershipAPIException, Exception {
        com.minus.mastatracksta.ShippingPackage.Package pkg;
        switch (origin) {
            case EBAY:
                pkg = new EBayPackage(trackingNumber);
                break;
            case ALIEXPRESS:
            case BANGGOD:
            case ZAMPLEBOX:
            case UNKNOWN:
            case UNSPECIFIED:
                pkg = new AliExpressPackage(trackingNumber);
            default:
                throw new AssertionError("unable to find item.");
        }

        return pkg;
    }
}
