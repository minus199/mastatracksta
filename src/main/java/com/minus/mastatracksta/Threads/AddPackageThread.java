package com.minus.mastatracksta.Threads;

import Classes.AftershipAPIException;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;
import Exceptions.trackingDriver.afterShip.MoreThanOneCourierDetectedException;
import com.minus.mastatracksta.Modules.Controllers.ItemFromKnownOriginController;
import com.minus.mastatracksta.ShippingPackage.Item.Item;
import com.minus.mastatracksta.Modules.ENUMS.OriginEnum;
import com.minus.mastatracksta.ShippingPackage.PackageFactory;
import com.minus.mastatracksta.ShippingPackage.Item.resolvers.ByOriginItemFactoryResolver;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author minus
 */
public class AddPackageThread extends OnFinishNotifyingThread {

    final private String trackingNumber;
    final private OriginEnum origin;
    final private ArrayList<ItemFromKnownOriginController.newItemInputPair> itemInputPairs;

    public AddPackageThread(String trackingNumber, OriginEnum origin, ArrayList<ItemFromKnownOriginController.newItemInputPair> itemInputPairs) {
        this.trackingNumber = trackingNumber;
        this.origin = origin;
        this.itemInputPairs = itemInputPairs;
    }

    protected com.minus.mastatracksta.ShippingPackage.Package createPackage(String trackingNumber) throws SQLException, BaseAftershipException, AftershipAPIException, MoreThanOneCourierDetectedException, IllegalAccessException, InstantiationException, Exception {
        return PackageFactory.create(trackingNumber, origin, processItems());
    }

    private ArrayList<Item> processItems() {
        ArrayList<Item> pendingItems = new ArrayList<>();

        itemInputPairs.forEach((ItemFromKnownOriginController.newItemInputPair pair) -> {
            if (pair.getItemCode().getText().length() > 0) {
                try {
                    Item item = ByOriginItemFactoryResolver.resolveAndCreate(pair.getItemCode().getText(), origin);
                    pendingItems.add(item);
                } catch (Exception ex) {
                    System.out.println("=====================================");
                    System.out.println("Unable to add item: " + ex.getMessage());
                    StackTraceElement[] elements = ex.getStackTrace();
                    System.out.println("Stack trace: ");
                    for (StackTraceElement element : elements) {
                        System.out.println("class: " + element.getClassName() + " file: " + element.getFileName() + " method: " + element.getMethodName() + "   " + element.getLineNumber());
                    }
                    System.out.println("=====================================");
                }
            }
        });

        return pendingItems;
    }

    @Override
    public void doRun() {
        try {
            createPackage(trackingNumber);
        } catch (Exception ex) {
            System.out.println("what the fuck");
        }
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public OriginEnum getOrigin() {
        return origin;
    }

    public ArrayList<ItemFromKnownOriginController.newItemInputPair> getItemInputPairs() {
        return itemInputPairs;
    }
}
