package com.minus.mastatracksta.Threads;

import Classes.AftershipAPIException;
import Classes.Tracking;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;
import com.minus.mastatracksta.Modules.Tracking.PackageTrackingManager;
import java.sql.SQLException;

/**
 * @author minus
 */
public class FetchTrackingThread extends OnFinishNotifyingThread {
    private Exception error;
    private Tracking tracking;
    private PackageTrackingManager packageTrackingManager;

    public FetchTrackingThread(PackageTrackingManager packageTrackingManager) {
        this.packageTrackingManager = packageTrackingManager;
    }

    @Override
    public void doRun() {
        try {
            fetchTracking();
        } catch (AftershipAPIException ex) {
            this.error = new BaseAftershipException(ex);
        } catch (Exception ex) {
            this.error = ex;
        }
    }

    private void fetchTracking() throws SQLException, Exception {
        this.tracking = packageTrackingManager.getTracking();
    }
    
    public Tracking getTracking() {
        return tracking;
    }

    public PackageTrackingManager getPackageTrackingManager() {
        return packageTrackingManager;
    }
    
    public Exception getError() {
        return error;
    }
    
    public Boolean hasError(){
        return error instanceof Exception;
    }
}
