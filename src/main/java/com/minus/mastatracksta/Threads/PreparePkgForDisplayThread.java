package com.minus.mastatracksta.Threads;

import Classes.AftershipAPIException;
import Exceptions.trackingDriver.afterShip.BaseAftershipException;
import com.minus.mastatracksta.ShippingPackage.Package;
import com.minus.mastatracksta.ShippingPackage.PackageFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author minus
 */
public class PreparePkgForDisplayThread extends OnFinishNotifyingThread {
    final private String trackingNumber;
    private Package pkg;
    
    public PreparePkgForDisplayThread(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }
    
    @Override
    public void doRun() {
        try {
            pkg = PackageFactory.getBy().trackingNumber(trackingNumber);
            pkg.process();
        } catch (IllegalAccessException ex) {
            Logger.getLogger(PreparePkgForDisplayThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(PreparePkgForDisplayThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BaseAftershipException ex) {
            Logger.getLogger(PreparePkgForDisplayThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AftershipAPIException ex) {
            Logger.getLogger(PreparePkgForDisplayThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PreparePkgForDisplayThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Package getPkg() {
        return pkg;
    }
}
