package com.minus.mastatracksta.Threads;

/**
 * @author minus
 */
public interface ThreadCompleteListener {

    void notifyOfThreadComplete(final Thread thread);
}
